let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles(['resources/assets/css/admin.css'], 'public/css/admin.css')
   .styles(['resources/assets/css/shoppingCart.css'], 'public/css/shoppingCart.css')
   .styles(['resources/assets/css/login.css'], 'public/css/login.css')
   .styles(['resources/assets/css/userInfo.css'], 'public/css/userInfo.css')
   .styles(['resources/assets/css/register.css'], 'public/css/register.css')
   .styles(['resources/assets/css/itemsView.css'], 'public/css/itemsView.css')
   .styles(['resources/assets/css/404.css'], 'public/css/404.css')
   .styles(['resources/assets/css/nav.css'], 'public/css/nav.css')
   .styles(['resources/assets/css/itemView.css'], 'public/css/itemView.css')
   .styles(['resources/assets/css/homePage.css'], 'public/css/homePage.css')
   .js(['resources/assets/js/admin.js'], 'public/js/admin.js')
   .js(['resources/assets/js/adminAdd.js'], 'public/js/adminAdd.js')
   .js(['resources/assets/js/analysis.js'], 'public/js/analysis.js')
   .js(['resources/assets/js/registration.js'], 'public/js/registration.js')
   .js(['resources/assets/js/shoppingCart.js'], 'public/js/shoppingCart.js')
   .js(['resources/assets/js/userEdit.js'], 'public/js/userEdit.js')
   .js(['resources/assets/js/shipmentAddress.js'], 'public/js/shipmentAddress.js')
   .js(['resources/assets/js/payment.js'], 'public/js/payment.js')
   // .sass('resources/assets/sass/app.scss', 'public/css')
   .version();;
