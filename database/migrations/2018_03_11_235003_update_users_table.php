<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->unsignedInteger('shoppingcart_id')->nullable();
            $table->unsignedInteger('access')->default('2');
            $table->foreign('shoppingcart_id')->references('id')->on('shoppingcarts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropForeign('users_shoppingcart_id_foreign');
            $table->drop('shoppingcart_id');
            $table->drop('access');
        });
    }
}
