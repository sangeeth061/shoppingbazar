$(document).ready(function(){
    $.ajax({
        url: 'product/analysis',
        method: 'GET',
        success: function(data) {
            console.log(data);
            var Item = [];
            var quantity = [];
            var arcColor = [];
            var hoverArcColor = [];
            
            var randomColors = function() {
               var r = Math.floor(Math.random() * 255);
               var g = Math.floor(Math.random() * 255);
               var b = Math.floor(Math.random() * 255);
               return "rgb(" + r + "," + g + "," + b + ")";
            };

            for(var i in data) {            
                Item.push(data[i].item.name);
                quantity.push(data[i].total);
                arcColor.push(randomColors());
                hoverArcColor.push(randomColors());
            }
            
            var chartdata = {
                labels: Item,
                datasets : [
                    {
                        label:"item",
                        backgroundColor: arcColor,
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        hoverBackgroundColor: hoverArcColor,
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: quantity
                    },
                ]
            };
            var ctx = $("#canvas");

            var barGraph = new Chart(ctx, {
                type: 'pie',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});