var ALPHABETS = new RegExp(/^[a-zA-Z ]+$/);
var NUMBERS = new RegExp(/^[0-9]+$/);
var EMAIL = new RegExp(/^[a-zA-Z][a-z A-Z 0-9]*@[a-zA-z]+\.[a-zA-z]+$/);
var hasError;

$(document).ready(function(){

    $("[name = 'firstname']").blur(function()
    {
        var firstname = $(this).val();

        if (firstname === "") 
        {
            $("#firstNameError").html("First name is required.");
            hasError = 1;
            return false;
        } 
        else if (!(ALPHABETS.test(firstname))) 
        {
            $("#firstNameError").html("Alphabets only.");
            $("[name = 'firstname']").focus();
            hasError = 1;
        }
        else
        {
            $("#firstNameError").html("");
        }
    });

    $("[name = 'email']").blur(function()
    {
        var email1 = $(this).val();

        if (email1 === "")
        {
            $("#emailError").html("Email is required.");
            $("[name = 'email']").focus();
            hasError = 1;
        } 
        else if (!(EMAIL.test(email1))) 
        {
            $("#emailError").html("Invalid email.");
            $("[name = 'email']").focus();
            hasError = 1;
        }
    });

    $("[name = 'password']").keyup(function()
    {
        var pwd = $(this).val();

        if (pwd === "") 
        {
            $("#passwordError").html("Password is required.");
            $("[name = 'password']").focus();
            hasError = 1;
        } 
        else if (pwd.length < 8) 
        {
            $("#passwordError").html("Min 8 characters.");
            $("[name = 'password']").focus();
            hasError = 1;
        }
        else
        {
            $("#passwordError").html("");
        }
    });

    $("[name = 'confirmpass']").keyup(function()
    {
        var pwd_confirm = $(this).val();

        if (pwd_confirm === "") 
        {
            $("#confirmPasswordError").html("Confirm the password.");
            $("[name = 'confirmpass']").focus();
            hasError = 1;
        } 
        else if (pwd_confirm !== $("[name = 'password']").val()) 
        {
            $("#confirmPasswordError").html("Passwords did NOT match");
            $("[name = 'confirmpass']").focus();
            hasError = 1;
        }
        else
        {
            $("#confirmPasswordError").html("");
        }
    });

    $("[name = 'address']").blur(function()
    {
        var address = $(this).val();

        if (address === "") 
        {
            $("#addressError").html("adress is required");
            $("[name = 'address']").focus();
            hasError = 1;
        } 
        else
        {
            $("#addressError").html("");
        }
    });

    $("[name = 'phone']").blur(function()
    {
        var phone = $(this).val();

        if (phone === "") 
        {
            $("#phoneError").html("phone is required.");
            $("[name = 'phone']").focus();
            hasError = 1;
        } 
        else if (phone.length != 10) 
        {
            $("#phoneError").html("invalid no");
            $("[name = 'phone']").focus();
            hasError = 1;
        }
        else if(!(NUMBERS.test(phone)))
        {
            $("#phoneError").html("invalid no");
            $("[name = 'phone']").focus();
            hasError = 1;
        }
        else
        {
            $("#phoneError").html("");
        }
    });
});

function validate() 
{
    var userName = $('#username').val();
    var firstName = $('#firstname').val();
    var lastName = $('#lastname').val();
    var email = $('#email').val();
    var gender = $('#gender').val();
    var password1 = $('#password').val();
    var password2 = $('#password_confirmation').val();
    var address = $('#address').val();
    var phoneno = $('#phnno').val();

    var hasError = false;

    if(userName === "")
    {
        $('#ue').html("usernames is required");
        hasError = true;
    }
    else
    {
        $('#ue').html("");
    }

    if(firstName === "")
    {
        $('#firstNameError').html("firstName is required");
        hasError = true;
    }
    else
    {
        $('#firstNameError').html("");
    }


    if(userName === "")
    {
        $('#userNameError').html("username is required");
        hasError = true;
    }
    else
    {
        $('#userNameError').html("");
    }

    if(!(ALPHABETS.test(firstName))) 
    {
        $('#firstNameError').html("Only Alphabets");
        hasError = true;
    }
    else
    {
        $('#firstNameError').html("");
    }

    if(email === "")
    {
        $('#emailError').html("Email is required");
        hasError = true;
    }
    else
    {
        $('#emailError').html("");
    }

    if (!(EMAIL.test(email))) 
    {
        hasError = true;
        $('#emailError').html("Invalid email");
    }
    else
    {
        $('#emailError').html("");
    }

    if(password1 === "")
    {
        hasError = true;
        $('#passwordError').html("Required");
    }
    else
    {
        $('#passwordError').html("");
    }

    if(password1 != password2)
    {
        hasError = true;
        $('#confirmPasswordError').html("Password didnt match");
    }
    else
    {
        $('#confirmPasswordError').html("");
    }

    if(address === "")
    {
        hasError = true;
        $('#addressError').html("Address is required");
    }
    else
    {
        $('#addressError').html("");
    }

    if(phoneno === "")
    {
        hasError = true;
        $('#phoneError').html("phone no required");
    }
    else
    {
        $('#phoneError').html("");
    }
    
    if( gender != 'M' && gender != 'F' && gender != 'O')
    {
        hasError = true;
        $('#genderError').html("Invalid Gender");
    }
    else
    {
        $('#genderError').html("");
    }


    return !hasError;
}