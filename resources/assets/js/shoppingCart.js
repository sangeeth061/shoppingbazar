 $(document).ready(function() {
 
 //to add the quantity
 $('button.add').click(function () {
    var target = $('.quantity', this.parentNode)[0];
    target.value = +target.value + 1;
    quantityUpdate();
    updateTotal();
});

//To decrease the quantity
 $('button.sub').click(function () {
    var target = $('.quantity', this.parentNode)[0];
    if (target.value > 1) {
        target.value = +target.value - 1;
        quantityUpdate();
        updateTotal();
    }
 });

 //To update total due to change in quantity
 var updateTotal = function () {

    var sum = 0;
    $(".costs").each(function () {
        var price = $(this).data('price');
        var quantity = $(this).parent().parent().find('#quantity').val();
        
        if(price > 0 && quantity > 0)
        {
            var subtotal = price * quantity;
            $(this).html(subtotal);
            sum += subtotal;
            $('strong.final').html(sum);
        }
    });
 };

 updateTotal();//To get intal total

//To update quantityin database
function quantityUpdate()
{   
    $(".quantity").each(function () {
        var quantityNo = $(this).val();
        var itemId = $(this).parent().find('#itemId').val();
             
        if(quantityNo > 0 && itemId > 0)
        {
            $.ajax({
                type : 'POST',
                url : 'quantity',
                data : {
                    quantity : quantityNo,
                    id : itemId,
                }, 

                success:function(data){
                    if(data.msg == "0")
                    {
                        window.location.replace("https://shoppingbazaar.com/public/cart");
                    }
                }
             });
            $('#error').html("");
        }
        else
        {
            $('#error').html("invalid input");
        }
    });
 }
 $('.quantity').on('input', function(){
    quantityUpdate();
    updateTotal();
    });
});
