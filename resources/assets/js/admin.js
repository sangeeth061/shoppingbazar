//Get data for the specific item

$(document).ready(function() {
    $('#name').on('input', function(){
        var itemName = $('#name option:selected').text();

        $.ajax({
            type : 'POST',
            url : 'update',
            data : {
                item : itemName,
            }, 

            success:function(data){
                var items = data.items;
                var cost = items['cost'];
                var description = items['description'];
                var category = items['category_id'];
                var id = items['id'];
                $('#cost').val(cost);
                $('#description').val(description);
                $('#category_id').val(category);
                $('#category_id option:selected').removeAttr('selected');
                $("#category_id option[value='"+category+"']").attr('selected', 'selected');  
                $('#itemId').val(id);
            },
         });
    });
});

//get the inormation about particular category
$(document).ready(function() {
    $('#category_id').on('input', function(){
        var itemName = $("#category_id option:selected").val();

        $.ajax({
            type : 'POST',
            url : 'updatecategory',
            data : {
                item : itemName,
            }, 

            success:function(data){
                var items = data.items;
                var name = [];
                var item;  
                for (item = 0; item < items.length; ++item) {
                    name[item] = items[item].name;
                }

                var select = document.getElementById('name');
                select.innerHTML = ''; 
                var fragment = document.createDocumentFragment();
                name.forEach(function(name) {
                    var option = document.createElement('option');
                    option.innerHTML = name;
                    option.value = name;
                    fragment.appendChild(option);
                });
                select.appendChild(fragment);
            }
         });
    });
});

// Validating before sending to server
var NUMBERS = new RegExp(/^[0-9]+$/);
$(document).ready(function() {
    $('#update').click(function(){
        
        if($("#name").val() == '1') { 
            $('#nameError').html('required');
            return false;
        }
        else
        {
            $('#nameError').html('');
        }
        
        if($("#description").val() == '') { 
            $('#descriptionError').html('required');
            return false;
        }
        else
        {
            $('#descriptionError').html('');
        }
        
        if($("#cost").val() == '') { 
            $('#costError').html('required');
            return false;
        }
        else if(!(NUMBERS.test($('#cost').val())))
        {
            $('#costError').html('only positive numbers');
            return false;
        }
        else
        {
            $('#costError').html('');
        }

    });
});
//To Delete specific item
$(document).ready(function() {
    $('#Delete').click(function(){
        var itemId = $('#itemId').val();
        $.ajax({
            type : 'POST',
            url : 'delete',
            data : {
                item : itemId,
            }, 
        success:function(data){
            window.location.replace('update');
        }
    });
    });
});