var NUMBERS = new RegExp(/^[0-9]+$/);
$(document).ready(function() {
    $('#create').click(function(){
        
        if($("#name").val() == '') { 
            $('#nameError').html('required');
            return false;
        }
        else
        {
            $('#nameError').html("");
        }
        
        if($("#description").val() == '') { 
            $('#descriptionError').html('required');
            return false;
        }
        else
        {
            $('#descriptionError').html('');
        }
        
        if($("#cost").val() == '') { 
            $('#costError').html('required');
            return false;
        }
        else if(!(NUMBERS.test($('#cost').val())))
        {
            $('#costError').html('only positive numbers');
            return false;
        }
        else
        {
            $('#costError').html('');
        }
        
        if($("#image").val() == '') { 
            $('#imageError').html('required');
            return false;
        }
        else
        {
            $('#imageError').html('');
        }
    });
});