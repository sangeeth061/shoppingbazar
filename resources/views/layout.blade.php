<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ShoppingBazar</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Bootstrap core CSS -->
        
        <link href="{{asset('css/layout.css')}}" rel="stylesheet">
        <!-- Custom styles for this template -->
        
        <link rel="shortcut icon" href="{{{ asset('staticpics/favicon.ico') }}}">
        
        <link href="{{asset('css/layout1.css')}}" rel="stylesheet">
        <link href="{{Mix('css/nav.css')}}" rel="stylesheet">

        @yield('styling')
    </head>

    <body>
    <div>
        @include('layouts.nav')

        @yield('slidecontent')

        @yield('content')

        </div>
        <!-- Footer -->
        <footer class="py-5 bg-dark fixed-bottom" style="height: 2px;">
          <div class="container">
            <p class="m-0 text-center text-white">Have A Wonderful Day &copy;Shopping Website</p>
          </div>
          <!-- /.container -->
        </footer>
<!--         <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
        <!-- Bootstrap core JavaScript -->
        <script src="{{asset('js/layout.js')}}"></script>
        <script src="{{asset('js/layout1.js')}}"></script>


        @yield('scripting')

        <script type="text/javascript">
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
        </script>
    </body>

</html>
