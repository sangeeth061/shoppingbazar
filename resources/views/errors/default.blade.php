@extends('../.layout')
@section('styling')
    <link href="{{asset('css/404.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="error-template">
            <h1 class="error">Some error occured</h1>
        </div>
    </div>
</div>
@endsection