@extends('../.layout')
@section('styling')
    <link href="{{asset('css/404.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="error-template">
            <h1 class="error">Oops!</h1>
            <h2 class="error1">Page Not Found</h2>
        </div>
    </div>
</div>
@endsection