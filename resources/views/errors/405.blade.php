@extends('../.layout')
@section('styling')
    <link href="{{asset('css/404.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="error-template">
            <h1 class="error">Permission</h1>
            <h2 class="error1">Denied</h2>
        </div>
    </div>
</div>
@endsection