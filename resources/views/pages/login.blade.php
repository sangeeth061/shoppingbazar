@extends('layout')
    
    @section('styling')
        <link href="{{Mix('css/login.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @endsection

    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    
    @if(count($errors) > 0)
        <div>
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{{$error}}</p>
            @endforeach
        </div>
    @endif()

    @section('content')
        <div>
            <div class="col-sm-6">
                @include('layouts.slideBar')
            </div>
            <div class="col-sm-6">
                <div class="log">
                    <div class="img-con">
                        <img src={{asset('staticpics/logo1.jpeg')}} alt="img not loaded" class="img-rounded">
                    </div>
                    {!! Form::open(['route' => 'logineed']) !!}
                        {{Form::label('email:','Email')}}
                        <span id="emailError"> </span>
                        {{Form::email('email',null,['placeholder' => 'enter your email', 'id' => 'email'])}}
                        {{Form::label('password:','Password')}}
                        <span id="passwordError"></span>
                        {{Form::password('password',['placeholder' => 'enter your password', 'id' => 'password'])}}
                        {{ Form::submit('Login', array('class' => 'btn btn-success', 'id' => 'login')) }}
                    {!! Form::close() !!}
                    <a href ="" data-toggle="modal" data-target="#exampleModal">Forgot password</a>
                    <hr>
                    <a href="login/google" class="fa fa-google">Sign in with google</a>
                    <a href="login/twitter" class="fa fa-twitter">Sign in with twitter</a>
                    <a href="login/oauth" class="fa fa-oauth">Login with Shopping karo</a>
                </div>
            </div>
        </div>
        @include('layouts.password')
    @endsection
    @section('scripting')
        <script src="{{asset('js/login.js')}}"></script>
    @endsection