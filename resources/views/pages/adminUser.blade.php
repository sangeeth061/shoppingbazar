@extends('layout')
@section('styling')
        <link href="{{Mix('css/admin.css')}}" rel="stylesheet">
@endsection
@Section('slidecontent')
<div class="container">

    <div class="row">

        <div class="col-lg-2">

            <h1 class="my-5">Admin</h1>
            <div class="list-group">
                <a href="{{route('item')}}" class="list-group-item">Add Products</a>
                <a href="{{route('update')}}" class="list-group-item">Update product</a>
                <a href="{{route('viewCart')}}" class="list-group-item">User Cart Information</a>
                <a href="{{route('home')}}" class="list-group-item">Shopping</a>
                <a href="{{route('analysis')}}" class="list-group-item">Sales</a>
            </div>
        </div>

        <div class="col-md-9">
        <h1>Users</h1>
        <ul>
        @foreach($items as $item)
           <li> <a href="cart/{{$item->id}}"> <h3>{{$item->user_name}}</h3></a></li>
        @endforeach
        </ul>
@endsection