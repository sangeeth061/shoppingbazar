@extends('layout')
@section('scripting')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
    <script src="{{Mix('js/analysis.js')}}"></script>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <h1 class="my-5">Admin</h1>
            <div class="list-group">
                <a href="{{route('item')}}" class="list-group-item">Add Products</a>
                <a href="{{route('update')}}" class="list-group-item">Update product</a>
                <a href="{{route('viewCart')}}" class="list-group-item">User Information</a>
                <a href="{{route('home')}}" class="list-group-item">Shopping</a>
                <a href="{{route('analysis')}}" class="list-group-item">Sales</a>
            </div>
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Sales summary</h2></div>
                <div class="panel-body">
                    <canvas id="canvas" height="280" width="600"></canvas>
                </div>
            </div>
        </div>
  </div>
</div>
@endsection