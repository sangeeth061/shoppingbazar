@extends('layout')
@section('styling')
    <link href="{{Mix('css/itemView.css')}}" rel="stylesheet">
@endsection
    @section('content')
                <div class="col-md-4 sidebar">
                    <img src ="{{ asset('storage/ItemImages/' . $items->image) }}"  alt = "sorry" class="imageSpecification">
                </div>
                <div class="col-md-8 description">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="poqw"> {{$items->name}}</h1>
                            <h3>Description</h3>
                            <h4>{{$items->description}}</h4>
                            <h3>Cost</h3>
                            <h4>&#x20b9;{{$items->cost}}</h4>
                            <a href="add/{{$items->id}}"> <button type="button" class="btn btn-success btn-lg">Add to Cart</button></a>
                            <a href={{route('home')}}> <button type="button" class="btn btn-primary btn-lg">Continue Shopping</button>
                            </a>
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exampleModal">
                                Review the product
                            </button>
                            @include('layouts.comment')
                        </div>

                        <div class="col-md-12 feedback">
                            <h2>Product Feedback</h2>
                            @forelse($comments as $comment)
                                <h3 class="kjew">{{$comment->review_point}}</h3>
                                <h2 class="jher">{{$comment->user->user_name}}</h2>
                                <h4 class="mnur">{{$comment->description}}</h4>
                                <hr>
                                 @empty
                                <h3>nocomments</h3>
                            @endforelse
                        </div>
                    </div>
                </div>

    @endsection