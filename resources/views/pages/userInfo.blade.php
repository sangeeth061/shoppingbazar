@extends('layout')
@section('styling')
    <link href="{{Mix('css/userInfo.css')}}" rel="stylesheet">
@endsection
@section('content')
<h4 class="txtColor"> Hi&nbsp{{$userDetails->user_name}}</h4>
<h4 class="txtColor">Your Basic details</h4>
<div class="col-sm-12 col-md-10 col-md-offset-1 log">
    <table class="table table-hover">
        <tr>
            <th>Username</th>
            <td>{{$userDetails->user_name}}</td>
        </tr>
        <tr>
            <th>Firstname</th>
            <td>{{$userDetails->first_name}}</td>
        </tr>
        <tr>
            <th>LastName</th>
            <td>{{$userDetails->last_name}}</td>
        </tr>
        <tr> 
            <th>Email</th>
            <td>{{$userDetails->email}}</td>
        </tr>
        <tr>
            <th>mobileno</th>
            <td>{{$userDetails->phone_no}}</td>
        </tr>
        <tr> 
            <th>address</th>
            <td>{{$userDetails->address}}</td>
        </tr>
    </table>
    <a href="{{route('editUser')}}"><button class="button">Edit</button></a>
</div>
@endsection
