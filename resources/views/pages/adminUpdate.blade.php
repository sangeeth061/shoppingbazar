@extends('layout')
@section('styling')
        <link href="{{Mix('css/admin.css')}}" rel="stylesheet">
@endsection
@Section('slidecontent')
<div class="container">

    <div class="row">
        <div class="col-lg-2">
         <div class="alert-danger"><span id="error" class="alert-danger"></span> </div>
            <h1 class="my-5">Admin</h1>
            <div class="list-group">
                <a href="{{route('item')}}" class="list-group-item">Add Products</a>
                <a href="{{route('update')}}" class="list-group-item">Update product</a>
                <a href="{{route('viewCart')}}" class="list-group-item">User Cart Information</a>
                <a href="{{route('home')}}" class="list-group-item">Shopping</a>
                <a href="{{route('analysis')}}" class="list-group-item">Sales</a>
            </div>
            @if(count($errors) > 0)
                    <div>
                            @foreach($errors->all() as $error)
                                    <p class="alert alert-danger">{{$error}}</p>
                            @endforeach
                    </div>
            @endif()
        </div>
        <div class="col-md-9 productContainer">
        <h3>Update Product</h3>
        {!! Form::open(['route' => 'updateItem', 'method' => 'POST', 'files' => true]) !!}
            <div class="form-group">
                <input type="hidden" name="itemId" id="itemId" value="">
            </div>

            <div class="form-group">
                {{ Form::label('category_id', 'Category')}}
                <span id="categoryError"></span>
                {{Form::select('category_id', ['0' => 'select', '1' => 'Dress', '2' => 'Watch', '3' => 'Electronics'], null ,['class' => 'form-control'])}}
            </div>
            
            <div class="form-group">
                @php
                    $item[1]="select something";
                @endphp
                {{ Form::label('name', 'Name')}}
                <span id="nameError"></span>
                {{Form::select('name',$item, 0,['class' => 'form-control', 'id' => 'name'])}}
            </div>
            
            <div class="form-group">
                {{ Form::label('description', 'Description') }}
                <span id="descriptionError"></span>
                {{ Form::text('description', null, ['class' => 'form-control', 'description']) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('cost', 'Price') }}
                <span id="costError"></span>
                <div class="input-group">
                    <div class="input-group-addon">&#x20b9;</div>
                    {{ Form::text('cost', null, ['class' => 'form-control', 'id' => 'cost']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('image', 'Image') }}
                <span id="imageError"></span>
                {{ Form::file('image',array('class' => 'form-control', 'id' => 'image')) }}
            </div>
            {{ Form::submit('Update', array('class' => 'btn btn-success', 'id' => 'update')) }}
            {{ Form::button('Delete', array('class' => 'btn btn-danger', 'id' => 'Delete')) }}
        {!! Form::close() !!}

        </div>
</div>
</div>
@endsection
@section('scripting')
    <script src="{{Mix('js/admin.js')}}"></script>
@endsection