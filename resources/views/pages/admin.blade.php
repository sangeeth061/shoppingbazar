@extends('layout')
@section('styling')
        <link href="{{Mix('css/admin.css')}}" rel="stylesheet">
@endsection
@Section('slidecontent')
<div class="container">
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
    <div class="row">
        <div class="col-lg-2">

            <h1 class="my-5">Admin</h1>
            <div class="list-group">
                <a href="{{route('item')}}" class="list-group-item">Add Products</a>
                <a href="{{route('update')}}" class="list-group-item">Update product</a>
                <a href="{{route('viewCart')}}" class="list-group-item">User Cart Information</a>
                <a href="{{route('home')}}" class="list-group-item">Shopping</a>
                <a href="{{route('analysis')}}" class="list-group-item">Sales</a>
            </div>
            @if(count($errors) > 0)
                    <div>
                            @foreach($errors->all() as $error)
                                    <p class="alert alert-danger">{{$error}}</p>
                            @endforeach
                    </div>
            @endif()
        </div>

        <div class="col-md-9 productContainer">
            <h3>Add Product</h3>
            {!! Form::open(['route' => 'additem', 'method' => 'POST', 'files' => true]) !!}

                <div class="form-group">
                    {{ Form::label('category_id', 'Category')}}
                    {{Form::select('category_id', ['1' => 'Dress', '2' => 'Watch', '3' => 'Electronics'], '1',['class' => 'form-control'])}}
                </div>

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    <span id="nameError"></span>
                    {{ Form::text('name', null, array('class' => 'form-control','id' => 'name')) }}
                </div>
                
                <div class="form-group">
                    {{ Form::label('description', 'Description') }}<span id="descriptionError"></span>
                    {{ Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) }}
                </div>
                    
                <div class="form-group">
                    {{ Form::label('cost', 'Price') }}<span id="costError"></span>
                    <div class="input-group">
                        <div class="input-group-addon">&#x20b9;</div>
                    {{ Form::text('cost', null, ['class' => 'form-control', 'id' => 'cost']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('image', 'Image') }}<span id="imageError"></span>
                    {{ Form::file('image',array('class' => 'form-control', 'id' => 'image')) }}
                </div>
                {{ Form::submit('Create', array('class' => 'btn btn-success', 'id' => 'create')) }}
            {!! Form::close() !!}
        </div>
</div>
</div>
@endsection
@section('scripting')
    <script src="{{Mix('js/adminAdd.js')}}"></script>
@endsection