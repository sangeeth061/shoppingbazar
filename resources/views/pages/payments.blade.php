@extends('layout')
@section('content')
<div class="container">
    <div class="col-xs-12 col-sm-8 " style="margin-top: 20px; ">
        <h2>Payment Options</h2><hr />
        {!! Form::open(['route' => 'checkout', 'method' => 'POST']) !!}
            <div class="form-group ">
                <input type="radio" name="type" id="creditcard" autocomplete="off" value="Creditcard"/>
                <div class="btn-group">
                    <label for="creditcard" class="btn btn-info active">
                    Creditcard
                    </label>
                </div>
            </div>
            <div class="form-group ">
                <input type="radio" name="type" id="Debitcard" autocomplete="off" value="Debitcard" />
                <div class="btn-group">
                    <label for="Debitcard" class="btn btn-info active">
                    Debitcard
                    </label>
                </div>
            </div>
            <div class="form-group ">
                <input type="radio" name="type" id="Cod" autocomplete="off" value="COD"   checked/>
                <div class="btn-group">
                    <label for="Cod" class="btn btn-info active">
                    Cash on delivery
                    </label>
                </div>
            </div>
            <div class="form-group ">
                <input type="radio" name="type" id="Netbanking" autocomplete="off" value="Netbanking" />
                <div class="btn-group">
                    <label for="Netbanking" class="btn btn-info active">
                    Netbanking
                    </label>
                </div>
            </div>
            <input type="submit" class="btn btn-danger"  name="regis" value="Pay&#x20b9;{{$total}}" id="payment">
    </div>
    <div class="col-xs-12 col-sm-4 " style="margin-top: 20px; border:2px solid; padding-bottom: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12) !important">
        <h2>Orders</h2><hr />
        <div class="container">
            <table class="table table-hover" style="background-color: darkseagreen">
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Cost</th>
                </tr>
                @foreach($items as $item)
                <tr>
                    <th>{{$item->item->name}}</th>
                    <th>{{$item->quantity}}</th>
                    <th>{{$item->item->cost * $item->quantity}}</th>
                </tr>
                @endforeach
                <tr>
                    <th>Total</th>
                    <th>{{$total}}</th>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripting')
    <script src="{{Mix('js/adminAdd.js')}}"></script>
@endsection