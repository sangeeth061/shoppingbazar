@extends('layout')
@section('styling')
    <link href="{{Mix('css/itemsView.css')}}" rel="stylesheet">
@endsection
    @section('slidecontent')
        @include('layouts.sideBar')
    @endsection
    @section('content')
    <div class="container">
    <div class="row itemAdjust">
        @forelse($products as $product)
            @include('layouts.electronicsView')
         @empty
            <h3>no Watches</h3>
        @endforelse
        </div>
        <span>{{ $products->links() }}</span>
        </div>
    @endsection