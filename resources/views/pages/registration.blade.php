@extends('layout')

@section('styling')
    <link href="{{Mix('css/register.css')}}" rel="stylesheet">
@endsection

@section('content')

    <h1>Registration page</h1>

    @if(count($errors) > 0)
        <div>
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{{$error}}</p>
            @endforeach
        </div>
    @endif()

    {!! Form::open(['route' => 'registerd', 'method' => 'POST', 'name' => 'test', 'id' => 'test', 
    'class' => 'form-horizontal container', 'onsubmit' => 'return validate()']) !!}

        <div class="form-group row">
            {{Form::label('firstname','Firstname*',['class' => 'col-sm-4 control-label', 'for' => 'firstname'])}}
            <div class="col-sm-8">
            {{Form::text('firstname', null, ['placeholder' => 'firstname','class' =>'form-control', 'id' => 'firstname'])}}
            <span id ="firstNameError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('lastname','Lastname',['class' => 'col-sm-4 control-label', 'for' => 'lastname'])}}
            <div class="col-sm-8">
            {{Form::text('lastname', null, ['placeholder' => 'lastname', 'class' =>'form-control', 'id' => 'lastname'])}}
            <span id ="le" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('email','Email*',['class' => 'col-sm-4 control-label', 'for' => 'email'])}}
            <div class="col-sm-8">
            {{Form::email('email', null, ['placeholder' => 'email', 'class' => 'form-control', 'id' => 'email'])}}
            <span id ="emailError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('password','Password*',['class' => 'col-sm-4 control-label', 'for' => 'password'])}}
            <div class="col-sm-8">
            {{Form::password('password', ['placeholder' => 'password','class' => 'form-control', 'id' => 'password'])}}
            <span id ="passwordError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('passwordconfirm','ConfirmPassword',['class' => 'col-sm-4 control-label', 'for' => 'password_confirmation'])}}
            <div class="col-sm-8">
            {{Form::password('password_confirmation', ['placeholder' => 'password', 'class' => 'form-control', 'id' => 'password_confirmation'])}}
            <span id ="confirmPasswordError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('username','Username*',['class' => 'col-sm-4 control-label', 'for' => 'username'])}}
            <div class="col-sm-8">
            {{Form::text('username', null, ['placeholder' => 'username', 'class' => 'form-control', 'id' => 'username'])}}
            <span id ="userNameError" class="highlight"> </span>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-4 control-label">Gender</label>
            <div class="col-sm-8">
            {{Form::select('gender', ['M' => 'male', 'F' => 'female', 'O' => 'others'], 'M',['class' => 'form-control', 'id' => 'gender'])}}
            <span id ="genderError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('address','Address*',['class' => 'col-sm-4 control-label', 'for' => 'address'])}}
            <div class="col-sm-8">
            {{Form::textarea('address', null, ['placeholder' => 'address', 'class' => 'form-control', 'id' => 'address'])}}
            <span id ="addressError" class="highlight"> </span>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('phoneno','Mobile*',['class' => 'col-sm-4 control-label', 'for' => 'phoneno'])}}
            <div class="col-sm-8">
            {{Form::number('phoneno', null, ['placeholder' => 'phoneno','class' =>'form-control', 'id' => 'phnno'])}}
            <span id ="phoneError" class="highlight"> </span>
            </div>
        </div>
        
        <div class="col-sm-12">
        <input type="submit" class="btn btn-primary" placeholder="REGISTER" name="regis" value="Register" id="register">
            <p style="float :left;">Already user ?<a href="login"> click here</a></p>
        </div>

    {!! Form::close() !!}
@endsection

@section('scripting')
    <script src="{{mix('js/registration.js')}}"></script>
@endsection