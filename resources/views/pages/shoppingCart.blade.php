@extends('layout')
    @section('styling')
        <link href="{{Mix('css/shoppingCart.css')}}" rel="stylesheet">
    @endsection
    @section('content')
    <div class="col-sm-12 col-md-11 col-md-offset-1">
                <table class="table table-hover XYZX">
                    <tr>
                        <th>Items</th>
                        <th>Quantity</th>
                        <th class="text-center">Total </th>
                    </tr>
                    @foreach($items as $item)
                        <tr>
                            <td class="col-sm-4 col-md-4">
                                <a class="pull-left" href="item/{{$item->item->id}}"> <img src="{{asset('../storage/ItemImages/'.$item->item->image)}}" class="rounded"> </a>
                                <div>
                                    <h4><a href="item/{{$item->item->id}}" id="name">{{$item->item->name}}</a></h4>
                                </div>
                            </td>
                            <td class="col-sm-2 col-md-4">
                            <span class="alert-danger" id="error"></span>
                                <input type="hidden" id="itemId" value="{{$item->item->id}}">
                                <button type="button" id="minus" class="sub">-</button>
                                <input type="text" name="quantity" id="quantity" value="{{$item->quantity}}" class="quantity">
                                <button type="button" id="plus" class="add">+</button>
                            </td>
                            <td class="col-sm-3 col-md-2 text-center">&#x20b9;<strong id="cost" data-price="{{$item->item->cost}}" class="costs">{{$item->item->cost}}</strong></td>
                            <td class="col-sm-3 col-md-2">
                                <a href="removeItem/{{$item->id}}"> <button type="button" class="btn btn-danger"> Remove</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3>&#x20b9;<strong id="total" class="final">{{$total}}</strong></h3></td>
                    </tr>
                    <tr>
                    <td>
                            <a href="home"> <button type="button" class="btn btn-success">Continue Shopping</button>
                            </a></td>

                        @if(!$items->isEmpty())
                        <td>
                            <a href="{{route('payments')}}"><button type="button" class="btn btn-success">
                                Checkout <span class=""></span>
                            </button></a></td>
                        @endif
                    </tr>
                </table>
            </div>
    @endsection
    @section('scripting')
        <script src="{{Mix('js/shoppingCart.js')}}"></script>
    @endsection