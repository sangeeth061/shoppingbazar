@extends('layout')
@section('styling')
    <link href="{{Mix('css/shoppingCart.css')}}" rel="stylesheet">
@endsection
    @section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="col-sm-12 col-md-10 col-md-offset-1">
    @php
    $i =0;
    @endphp
                <table class="table table-hover XYZX">
                    <tr>
                        <th>Order History</th>
                        <th>Shipping Status</th>
                        <th>Payment</th>
                        <th>Quantity</th>
                        <th> Order Amount</th>
                    </tr>
                    @foreach($items as $item)
                        <tr>
                            <td class="col-sm-2 col-md-2 col-md-offset-1">
                                <a class="pull-left" href="item/{{$item->item->id}}"> <img src="{{asset('../storage/ItemImages/'.$item->item->image)}}"  class="rounded"> </a>
                                <div>
                                    <h4><a href="item/{{$item->item->id}}" id="name">{{$item->item_name}}</a></h4>
                                    {{$item->item->description}}
                                </div>
                            </td>
                            <td class="col-sm-2 col-md-2">
                            @php
                            if($shipmentStatus[$i]->status == '1')
                                echo"shipped";
                            else
                                echo "on the way";
                            @endphp
                            </td>
                            <td class="col-sm-2 col-md-2">{{$paymentStatus[$i++]->created_at}}</td>
                            <td class=class="col-sm-2 col-md-2">
                                <input type="hidden" id="itemId" value="{{$item->item->id}}">
                                <span name="quantity" class="quantity">{{$item->quantity}}</span>
                            </td>
                            <td class="col-sm-2 col-md-2 col-md-offset-1" text-center">&#x20b9;<strong id="cost" data-price="{{$item->item_cost}}" class="costs">{{$item->item_cost * $item->quantity}}</strong></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>
                                <a href="home"> <button type="button" class="btn btn-success">Continue Shopping</button>
                                </a>
                        </td>
                    </tr>
                </table>
            </div>
    @endsection