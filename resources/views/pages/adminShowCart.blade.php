@extends('layout')
    @section('styling')
        <link href="{{Mix('css/shoppingCart.css')}}" rel="stylesheet">
    @endsection
    @section('content')
    <div class="col-sm-12 col-md-11 col-md-offset-1">
                <table class="table table-hover">
                    <tr>
                        <th>Items</th>
                        <th>Quantity</th>
                        <th class="text-center">Total </th>
                    </tr>
                    @foreach($items as $item)
                        <tr>
                            <td class="col-sm-4 col-md-4">
                                <a class="thumbnail pull-left" href="#"> <img src="{{asset('../storage/ItemImages/'.$item->item->image)}}" class="rounded"> </a>
                                <div>
                                    <h4><a href="#" id="name">{{$item->item->name}}</a></h4>
                                </div>
                            </td>
                            <td class="col-sm-2 col-md-4">
                            <span>{{$item->quantity}}</span>
                            </td>
                            <td class="col-sm-3 col-md-2 text-center">&#x20b9;<strong id="cost" data-price="{{$item->item->cost}}" class="costs">{{$item->item->cost}}</strong></td>
                        </tr>
                    @endforeach
     

                    <tr>
                    <td>
                            <a href="{{route('item')}}"> <button type="button" class="btn btn-success">Back</button>
                            </a></td>
                    </tr>
                </table>
            </div>
    @endsection