@extends('layout')
    @section('styling')
        <link href="{{asset('css/homePage.css')}}" rel="stylesheet">
    @endsection
    @section('slidecontent')
        @include('layouts.sideBar')
        @include('layouts.slideBar')
    @endsection
    @section('content')
        <div style="margin-bottom: 10px;">
        @include('layouts.display',['image' => 'staticpics/electronic2.jpeg', 'ItemDescription' => 'electronics'])
        @include('layouts.display',['image' => 'staticpics/watch4.jpeg', 'ItemDescription' => 'watches'])
        @include('layouts.display',['image' => 'staticpics/shirt6.jpeg', 'ItemDescription' => 'dress'])
    @endsection