@extends('layout')
@section('styling')
    <link href="{{Mix('css/userInfo.css')}}" rel="stylesheet">
@endsection
@section('content')
@if(count($errors) > 0)
    <div>
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{$error}}</p>
        @endforeach
    </div>
@endif()
    <div class="container">                
        <h3 align="center" class="pad">UPDATE THE PERSONAL INFO </h3>
        {!! Form::open(['route' => 'changeUserInfo', 'method' => 'POST', 'name' => 'edit', 'id' => 'edit', 
        'class' => 'form-horizontal container',]) !!}
            <div class="form-group row pad">
                {{Form::label('firstname','Firstname',['class' => 'col-sm-2 control-label', 'for' => 'firstname'])}}
                <div class="col-sm-10">
                <Span id = "firstNameError" class ="alert-danger"></Span>
                {{Form::text('firstname', $userDetails->first_name, ['placeholder' => 'firstname','class' =>'form-control', 'id' => 'firstname'])}} 
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('lastname','Lastname',['class' => 'col-sm-2 control-label', 'for' => 'lastname'])}}
                <div class="col-sm-10">
                {{Form::text('lastname', $userDetails->last_name, ['placeholder' => 'lastname','class' =>'form-control', 'id' => 'lastname'])}} 
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('username','Username',['class' => 'col-sm-2 control-label', 'for' => 'username'])}}
                <div class="col-sm-10">
                <Span id = "userNameError" class ="alert-danger"></Span>
                {{Form::text('username', $userDetails->user_name, ['placeholder' => 'username','class' =>'form-control', 'id' => 'username'])}}    
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('email','Email',['class' => 'col-sm-2 control-label', 'for' => 'email'])}}
                <Span id = "emailError"></Span>
                <div class="col-sm-10">
                {{Form::text('email', $userDetails->email, ['placeholder' => 'email','class' =>'form-control', 'id' => 'email'])}}    
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('mobileno','Mobileno',['class' => 'col-sm-2 control-label', 'for' => 'mobileno'])}}
                <div class="col-sm-10">
                <Span id = "phoneError" class="alert-danger"></Span>
                {{Form::text('mobileno', $userDetails->phone_no, ['placeholder' => 'mobileno','class' =>'form-control', 'id' => 'mobileno'])}}  
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('address','Address',['class' => 'col-sm-2 control-label', 'for' => 'address'])}}
                <div class="col-sm-10">
                <Span id = "addressError" class = "alert-danger"></Span>
                {{Form::textarea('address', $userDetails->address, ['placeholder' => 'address', 'class' => 'form-control', 'id' => 'address', 'rows' => 4,])}}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="update">continue</button>
                </div>
            </div>
            </form>
@endsection
@section('scripting')
    <script src="{{Mix('js/userEdit.js')}}"></script>
@endsection