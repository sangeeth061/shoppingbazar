@extends('layout')
@section('styling')
    <link href="{{Mix('css/itemsView.css')}}" rel="stylesheet">
@endsection
    @section('slidecontent')
        @include('layouts.sideBar')
    @endsection
    @section('content')
    </div>
    <div class="row itemAdjust">
        @forelse($products as $product)
            @include('layouts.searchView')
         @empty
        <h3 style="padding-top: 10px">No results found</h3>
    </div>
    @endforelse
    @endsection