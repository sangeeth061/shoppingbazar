@extends('layout')
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Shipping Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="exit">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(count($errors) > 0)
                    <div>
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    </div>
                @endif()
                {!! Form::open(['route' => 'updateAddress', 'method' => 'POST', 'id' =>'shippingAddress', 'files' => true]) !!}
                <div class="form-group">
                    {{ Form::label('address', 'Address')}}
                    {{ Form::textarea('address', null, ['class' => 'form-control', 'id' => 'address']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('phoneno', 'Phone')}}
                    {{ Form::text('phoneno', null, ['class' => 'form-control', 'id' => 'phoneno']) }}
                </div>
                {{ Form::submit('submit', array('class' => 'btn btn-success', 'id' => 'submit')) }}
                {!! Form::close() !!}
                </div>
            </div>
<!--             <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" class="exit"  >Close</button>
            </div> -->
        </div>
    </div>
</div>
@endsection
@section('scripting')
    <script src="{{Mix('js/shipmentAddress.js')}}"></script>
@endsection
