<div class="col-lg-4 col-md-6 mb-4">
    <div class="card h-1 productContainer">
        <center><a href="item/{{$product->id}}"><img class="card-img-top" src="{{ asset('../storage/ItemImages/' . $product->image) }}" alt="" class="imageSpecification"></a></center>
        <div class="card-body">
            <h2 class="card-title"> <a href="item/{{$product->id}}">{{$product->name}}</a> </h2>
            <h3>Cost</h3>
            <h5>&#x20b9;{{$product->cost}}</h5>
            <h4>Description</h4><p class="card-text text-truncate ">{{$product->description}}</p>
        </div>
    </div>
</div>