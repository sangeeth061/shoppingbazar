<div class="col-lg-4 col-md-6 mb-4 slideSettlement">
    <div class="card h-1">
        <center><a href="{{route($ItemDescription)}}"><img class="card-img-top" src="{{$image}}"></a></center>
        <div class="card-body">
            <h4 class="card-title">{{title_case($ItemDescription)}}</h4>
        </div>
    </div>
</div>