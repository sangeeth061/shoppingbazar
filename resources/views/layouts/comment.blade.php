<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'addComment', 'method' => 'POST', 'files' => true]) !!}
                <div class="form-group">
                    {{ Form::label('review_point', 'Experience')}}
                    {{ Form::text('review_point', null, ['class' => 'form-control', 'id' => 'review_point']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('Description', 'Description')}}
                    {{ Form::text('Description', null, ['class' => 'form-control', 'id' => 'Description']) }}
                </div>
                <input type="hidden" value="{{$items->id}}" name="productId">
                {{ Form::submit('submit', array('class' => 'btn btn-success', 'id' => 'submit')) }}
                {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>