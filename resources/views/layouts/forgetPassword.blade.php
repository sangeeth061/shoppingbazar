@extends('layout')
@section('content')
<div class="container">
@if(count($errors) > 0)
    <div>
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{$error}}</p>
        @endforeach
    </div>
@endif()
    <div class="panel panel-primary" style="margin-top: 20px;">
        <div class="panel-heading">ChangePassword</div>
        <form action="" method="Post">
            {{ csrf_field() }}
            <div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Email</label>
                    <div class="col-md-6 col-md-offset-2">
                        <input id="email" name="email_id" type="email" placeholder="enter your email" class="form-control input-md" value="{{$email}}" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Password</label>
                    <div class="col-md-6 col-md-offset-2">
                        <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password_confirmation">Confirm-Password</label>
                    <div class="col-md-6 col-md-offset-2">
                        <input id="password_confirmation" name="password_confirmation" type="password" placeholder="Password" class="form-control input-md">
                    </div>
                </div>
            </div>
            <div class="panel-footer"><input type="submit" class="btn btn-success"
            name=""></div>
        </form>
    </div>
</div>
@endsection