<div class="container">

  <div class="row">

    <div class="col-lg-3">

      <h1 class="my-4">Categories</h1>
      <div class="list-group">
        <a href="{{route('electronics')}}" class="list-group-item">Electronics</a>
        <a href="{{route('watches')}}" class="list-group-item">Watches</a>
        <a href="{{route('dress')}}" class="list-group-item">Dresses</a>
      </div>

    </div>