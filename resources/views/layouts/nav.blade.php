<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{route('home')}}"><h2 class="xjwe">SHOPPING BAZAR</h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse navew" id="navbarResponsive">
            @if(Auth::check())
                <form action={{route('search')}} method="POST">
                    {{ csrf_field() }}
                    <div class="input-group" style="width:90%;">
                        <input type="text" class="form-control" id="search-box" name="search"
                            placeholder="Search product" style="width: 259.5px;"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default search-btn" style="width: 34px;height: 34px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                </form>
            @endif
            <ul class="navbar-nav nav-pills ml-auto">
                @php
                    $fullUrl = $_SERVER['REQUEST_URI'];
                    $lastWord = substr($fullUrl, strrpos($fullUrl, '/') + 1);
                @endphp
            
                @if(Auth::check())
                    <li class="nav-item" tabindex="1">
                        <h3><a class="navbar-brand" href="{{route('user')}}"><h2>Hi</h2><strong>{{Auth::user()->user_name}}</strong></a></h3>
                    </li>
                    <li class="nav-item" tabindex="2">
                        <h4><a class="nav-link {{ ($lastWord == 'logout') ? 'active' : ''}} tdfs" href="{{route('logout')}}">Logout</a></h4>
                    </li>
                    <li class="nav-item" tabindex="3">
                        <h4><a class="nav-link {{ ($lastWord == 'cart') ? 'active' : ''}} tdfs" href="{{route('cart')}}">Cart</a></h4>
                    </li>
                    <li class="nav-item" tabindex="4">
                        <h4><a class="nav-link {{ ($lastWord == 'order') ? 'active' : ''}} tdfs" href="{{route('order')}}">My Orders</a></h4>
                    </li>
                    <li class="nav-item" tabindex="4">
                        <h4><a class="nav-link {{ ($lastWord == 'orderAddress') ? 'active' : ''}} tdfs" href="{{route('getAddress')}}">Shipping address</a></h4>
                    </li>
                   
                    @if(Auth::user()->access == '1')
                    <li class="nav-item" tabindex="5">
                        <h4><a class="nav-link {{ ($lastWord == 'create' || $lastWord == 'update' || $lastWord == 'product') ? 'active' : ''}} tdfs" href="{{route('item')}}">Admin</a></h4>
                    </li>
                    @endif

                @else    
                    <li class="nav-item" tabindex="6">
                        <h4><a class="nav-link {{ ($lastWord == 'register') ? 'active' : ''}} tdfs" href="{{route('register')}}">Register</a></h4>
                    </li>
                    <li class="nav-item" tabindex="7">
                        <h4><a class="nav-link {{ ($lastWord == 'login') ? 'active' : ''}} tdfs" href="{{route('login')}}">Login</a></h4>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>