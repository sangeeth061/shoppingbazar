<html>
<header>
    <link href="{{asset('css/layout.css')}}" rel="stylesheet">
    <link href="{{asset('css/layout1.css')}}" rel="stylesheet">
    <link href="{{asset('css/layout2.css')}}" rel="stylesheet">

</header>
<body>
    <div class="col-sm-12 col-md-11 col-md-offset-1">
        <h1>Hi<h2>{{$username}}</h2></h1>
        <table class="table table-hover" style="margin-bottom: 70px;">
            <tr>
                <th>Order</th>
                <th>Quantity</th>
                <th>Order amount</th>
            </tr>
            @foreach($items as $item)
            <tr>
                <div>
                    <h4><a href="#" id="name">{{$item->item->name}}</a></h4>
                </div>                            
                <td class="col-sm-2 col-md-4" style="text-align: center">
                <span name="Quantity" class="quantity">{{$item->quantity}}</span>
                </td>
                <td class="col-sm-3 col-md-2 text-center"><h2>&#x20b9;<strong id="cost" class="costs">{{$item->item->cost * $item->quantity}}</strong></h2></td>
            </tr>
            @endforeach
        </table>
    </div>
</body>
</html>