<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;
use Exception;

class Orderproduct extends Model
{
    protected $table = 'order_products';

    /**
     * To Get user details
     *
     * @return object
     */
    public function user()
    {
        try {
            return $this->belongsTo(User::class);
        } catch(Exception $e) { 
            Log::error( 'Error in order method of Orderproduct model: ' . $e->getMessage()); 
        }
    }

    /**
     * To Get items
     *
     * @return object
     */
    public function item()
    {
        try {
    	   return $this->belongsTo(Item::class);
        } catch(Exception $e) { 
            Log::error( 'Error in item method of Item model: ' . $e->getMessage()); 
           
            return null;
        }
    }

    /**
     * To add products to a order
     *
     * @param Integer $userId 
     * @param Integer $cost
     * @param Integer $quantity
     *
     * @return object
     */
    public static function addOrderProduct($name, $id, $cost, $quantity)
    {
        try {
        	$orderItem = new Orderproduct();
        	$orderItem->item_name = $name;
        	$orderItem->item_id = $id;
        	$orderItem->item_cost = $cost;
        	$orderItem->quantity = $quantity;
        	$orderItem->save();

            return $orderItem;
        } catch(Exception $e) { 
            Log::error( 'Error in addOrderProduct method of Show model: ' . $e->getMessage()); 

            return null;
        }
    }

    /**
     * To analysis the sales of items
     *
     * @return Json
     */
    public static function productAnalysis()
    {
        $data = Orderproduct::with(['item' => function($query) {
                        $query->select(['id', 'name']);
                    }])
                    ->select('item_id', DB::raw('SUM(quantity) as total'))
                    ->groupBy('item_id')->get()->toArray();
        
        return response()->json($data);

    }


    /**
     * To analysis the sales of items
     *
     * @return Json
     */
    public static function getOrderDetailsForApi($id)
    {
        try {
            return Orderproduct::where('user_id', $id)
                    ->select(['item_name', 'item_cost', 'quantity'])
                    ->get();
        } catch(Exception $e) { 
            Log::error( 'Error in getOrderDetailsForApi method of Orderproduct model: ' . $e->getMessage()); 

            return null;
        }
    }

    /**
     * To get id of sepecific product name and userId
     *
     * @return object
     */
    public static function getId($itemName, $userId)
    {
        return Orderproduct::where('item_name', $itemName)
                            ->where('user_id', $userId)
                            ->select('id')
                            ->get();
    }
}