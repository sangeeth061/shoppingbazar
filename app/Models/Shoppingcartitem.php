<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Log;

class Shoppingcartitem extends Model
{
    protected $table = 'shopping_cart_items';
    
    /**
     * To get cart details
     *
     * @return object
     */
        public function user()
    {
        try {
            return $this->belongsTo(User::class);
        } catch(Exception $e) { 
            Log::error( 'Error in cart method of Shoppingcartitem model: ' . $e->getMessage()); 

            return null;
        }
    }

    /**
     * To Get item details
     *
     * @return object
     */
    public function item()
    {
        try {
            return $this->belongsTo(Item::class)
                        ->select(array( "id",
                                    "name",
                                    "description",
                                    "image",
                                    "cost"
                                    ));
        } catch(Exception $e) { 
            Log::error( 'Error in item method of Shoppingcartitem model: ' . $e->getMessage()); 
            return null;
        }
    }

    /**
     * To add items to cart
     *
     * @param Integer $productId
     * @param Integer $cartId
     *
     * @return object
     */
    public static function addItemsTocart($productId, $cartId)
    {
        try {
            $item = new Shoppingcartitem();
            $item->item_id = $productId;
            $item->user_id = $cartId;
            $item->save();

            return $item;
        } catch(Exception $e) { 
            Log::error( 'Error in addItemsTocart method of Shoppingcartitem model: ' . $e->getMessage()); 
            return null;
        }
    }

    /**
     * To update quantity
     *
     * @param Integer $itemId
     * @param Integer $itemQuantity
     * @param Integer $shoppingCartId
     *
     * @return 1/0
     */
    public static function changeQuantity($shoppingCartId, $itemQuantity, $itemId)
    {
        try {
            Shoppingcartitem::where('user_id', $shoppingCartId)
                      ->where('item_id', $itemId)
                      ->update(['quantity' => $itemQuantity]);
                      return 1;
        } catch(Exception $e) {
            Log::error( 'Error in changeQuantity method of Shoppingcartitem model: ' . $e->getMessage()); 
            return 0;
        }
    }

    /**
     * To view cart
     *
     * @param Integer $itemId
     *
     * @return null
     * @return object
     */
    public static function viewCartDetails($itemId)
    {
        try {
            return Shoppingcartitem::where('user_id', '=', $itemId)->get();
        } catch(Exception $e) { 
            Log::error( 'Error in viewCartDetails method of Shoppingcartitem model: ' . $e->getMessage());

            return null; 
        }
    }

    /**
     * To analysis the cart of user
     *
     * @return Json
     */
    public static function getCartDetailsForApi($id)
    {
        try {
            return Shoppingcartitem::where('user_id', $id)
                    ->select(['item_id', 'created_at'])
                    ->get();
        } catch(Exception $e) { 
            Log::error( 'Error in getOrderDetailsForApi method of Orderproduct model: ' . $e->getMessage()); 

            return null;
        }
    }
}