<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use Exception;
use Log;

class Shipment extends Model
{
    protected $table = 'shipments';

    /**
     * To verify shipment
     *
     * @param Integer $orderId
     * @param integer $orderItem
     *
     * @return object
     */
    public static function shipmentVerifier($userId, $orderItem)
    {
        try{
            return Shipment::where('user_id', $userId)->where('orderproduct_id', $orderItem)->select(['id'])->first();
        }catch(Exception $e) { 
            Log::error( 'Error in shipmentVerifier method of Shipment model: ' . $e->getMessage());

            return null; 
        }
    }

    /**
     * To add shipment
     *
     * @param Integer $orderId
     * @param Integer $orderItem
     *
     * @return object
     */
    public static function addShipment($userId, $orderItem)
    {
        try {
            $shipItem = new Shipment();
            $shipItem->status = '0';
            $shipItem->user_id = $userId;
            $shipItem->orderproduct_id = $orderItem;
            $shipItem->save();

            return $shipItem;
        } catch(Exception $e) { 
            Log::error( 'Error in addShipment method of Shipment model: ' . $e->getMessage());
            
            return null;
        }
    }

    /**
     * To update shipment status
     *
     * @param void
     *
     * @return void
     */
    public static function makeShipment()
    {
        try {
            Shipment::where('created_at', '<', Carbon::now()->subMinutes(40))
                    ->update(['status'=>'1']);
        } catch(Exception $e) {
            Log::error('Error in makeShipment method of Shipment model' . $e->getMessage());

            return null;
        }
    }
}