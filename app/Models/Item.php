<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Log;

class Item extends Model
{

    protected $fillable = 
    [
        'name', 
        'cost', 
        'image', 
        'description',
        'category_id',
    ];

    /**
     * To get categorie
     *
     * @return object
     */
    Public function category()
    {
        try {
            return $this->hasMany(Category::class);
        } catch(Exception $e) { 
            Log::error( 'Error in category method of Item model: ' . $e->getMessage()); 
            
            return 0;
        }
    }

    /**
     * To search for given Item
     *
     * @param String $searchItem
     *
     * @return object
     */
    public static function search($searchItem)
    {
        $items = $searchItem;
        try {   
            return Item::where('name', 'like', '%'.$items.'%')->select(['name', 'cost', 'image', 'id', 'description'])->get();
        } catch(Exception $e) {   
            Log::error( 'Error in search method of Item model: ' . $e->getMessage());
            
            return null;
        }
    }

    /**
     * To get items of specific category
     *
     * @param Integer category_id
     *
     * @return object
     */
    public static function viewCategory($category_id)
    {
        try {
            return Item::where('category_id', $category_id)
                        ->select([
                            'name',
                            'cost',
                            'image',
                            'id', 
                            'description'
                            ])
                        ->paginate(6);
        } catch(Exception $e) { 
            Log::error( 'Error in viewCategorie method of Item model: ' . $e->getMessage());
            
            return null; 
        }
    }

    /**
     * To get reviews for the item
     *
     * @return object
     */
    public function reviews()
    {
        return $this->hasMany(Comment::class)
                    ->select([
                                "id",
                                "item_id",
                                "description",
                                "review_point",
                                "user_id",
                                ]);
    }

    /**
     * To Get details of particular item
     *
     * @param Integer $id
     *
     * @return object
     */
    public static function getDetails($id)
    {
        try{
            return Item::where('id', $id)
                            ->select(['image', 'cost'])
                            ->first();
        } catch(Exception $e) { 
            Log::error( 'Error in getDetails method of Item model: ' . $e->getMessage());
            
            return null; 
        }
    }

    /**
     * To update given Item
     *
     * @param Integer $id
     * @param Integer $category_id
     * @param Integer $cost
     * @param String $description
     * @param String $image
     *
     * @return object
     */
    public static function itemUpdate($id, $description, $cost, $category_id, $image)
    {
        try {
            return Item::where('id', $id)
                            ->update([
                                'description' => $description,
                                'cost' => $cost,
                                'category_id' => $category_id,
                                'image' => $image
                                ]);
        } catch(Exception $e) { 
            Log::error( 'Error in itemUpdate method of Item model: ' . $e->getMessage());
            
            return null; 
        }
    }
    
    /**
     * To get details for given Item
     *
     * @param String $itemName
     *
     * @return object
     */
    public static function getItemDetails($itemName)
    {
        try {
            return Item::where('name', $itemName)
                        ->select([
                        'id',
                        'name',
                        'cost',
                        'category_id',
                        'image',
                        'description'
                        ])
                        ->first();
        } catch(Exception $e) { 
            Log::error( 'Error in getItemDetails method of Item model: ' . $e->getMessage());
            
            return null; 
        }
    }

    /**
     * To view given Item
     *
     * @param Integer $productId
     *
     * @return object
     */
    public static function viewProduct($productId)
    {
        try {
            return Item::where('id', $productId)
                        ->select([
                            'name', 
                            'cost', 
                            'image', 
                            'id', 
                            'description'
                        ])
                        ->first();
        } catch(Exception $e) { 
            Log::error( 'Error in viewProduct method of Item model: ' . $e->getMessage()); 

            return null;
        }
    }

    /**
     * To get specific categories product
     *
     * @param Integer $productId
     *
     * @return object
     */
    public static function viewCategorieProducts($category_id)
    {
        try {
            return Item::where('category_id', $category_id)->get();
        } catch(Exception $e) { 
            Log::error( 'Error in viewCategorie method of Item model: ' . $e->getMessage());
            
            return null; 
        }
    }
}