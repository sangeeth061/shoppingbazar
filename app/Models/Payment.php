<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Log;

class Payment extends Model
{

   /**
    * To Check for payment
    *
    * @param Integer $orderId
    * @param Integer $orderItem
    *
    * @return object
    */
   public static function paymentVerifier($userId, $orderItem)
   {
        try {
            return Payment::where('user_id', $userId)->where('orderproduct_id', $orderItem)->select(['id'])->first();
        } catch(Exception $e) { 
            Log::error( 'Error in paymentVerifier method of Payment model: ' . $e->getMessage()); 
            
            return null;
        }
   }

   /**
    * To add payment
    *
    * @param Integer $orderId
    * @param Integer $orderItem
    * @param String $type
    *
    * @return object
    */
   public static function addPayment($userId, $orderItem, $type)
   {
        try {
            $payItem = new Payment();
            $payItem->status = '1';
            $payItem->type = $type;
            $payItem->user_id = $userId;
            $payItem->orderproduct_id = $orderItem;
            $payItem->save();
            
            return $payItem;
        } catch(Exception $e) 
        { 
            Log::error( 'Error in addPayment method of Payment model: ' . $e->getMessage()); 
            
            return null;
        }
   }
}