<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Log;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable=['name',];
    
    /**
     * To view items 
     *
     * @access public
     * @return object
     */
    public function items()
    {
    	try {
        	return $this->hasMany(Item::class);
        } catch(Exception $e) { 
            Log::error( 'Error in items method of Category model: ' . $e->getMessage()); 
            
            return null;
        }
    }
}
