<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = 
    [
        'first_name',
        'email',
        'password',
        'email_verified',
        'user_name',
        'last_name',
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden =
    [
        'password',
        'remember_token',
    ];

    /**
     * To get cart details
     *
     *
     * @return object
     */
    public function cartItems()
    {
        try {
            return $this->hasMany(Shoppingcartitem::class)
                        ->select(array( "id",
                                        "item_id",
                                        "quantity",
                                        "user_id",
                                        ));
        } catch(Exception $e) { 
            Log::error( 'Error in cartItem method of Shoppingcart model: ' . $e->getMessage()); 
           
            return null;
        }
    }

    /**
     * To review an item
     *
     *
     * @return object
     */
    public function review()
    {
        return $this->hasMany(Comment::class)
                    ->select(array( "id",
                                    "item_id",
                                    "description",
                                    "review_point",
                                    "user_id",
                                    ));
    }

    /**
     * To Get details for given Order
     *
     * @return null
     * @return object
     */
    public function orderItems()
    {
        try{
            return $this->hasMany(Orderproduct::class);
        } catch(Exception $e) { 
            Log::error( 'Error in orderItems method of Order model: ' . $e->getMessage());
            
            return null; 
        }
    }

    /**
     * To Get shipmentdetails for given Order
     *
     * @return null
     * @return object
     */
    public function ShipItems()
    {
        try{
            return $this->hasMany(Shipment::class);
        } catch(Exception $e) { 
            Log::error( 'Error in cartItem method of Shoppingcart model: ' . $e->getMessage()); 
           
            return null;
        }
    }

    /**
     * To Get paymentdetails for given Order
     *
     * @return null
     * @return object
     */
    public function payItems()
    {
        try {
            return $this->hasMany(Payment::class);
        } catch(Exception $e) { 
            Log::error( 'Error in cartItem method of Shoppingcart model: ' . $e->getMessage()); 
           
            return null;
        }
    }

    /**
     * To add customer to database
     *
     * @param object $user
     * @param http request object
     *
     * @return object
    */
    public static function addCustomer($request, $user, $token)
    {  
        try {
            $user->user_name = $request['username'];
            $user->last_name = $request['lastname'];
            $user->first_name = $request['firstname'];
            $user->password = Hash::make($request['password']);
            $user->address = $request['address'];
            $user->email = $request['email'];
            $user->phone_no = $request['phoneno'];
            $user->token = $token;
            $user->save();

            return $user;
        } catch(Exception $e) { 
            Log::error( 'Error in addCustomer method of User model: ' . $e->getMessage()); 
            
            return null;
        }
    }

    /**
     * To verify email
     *
     * @param Integer $itemId
     *
     * @return 1
     * @return object
     */
    public static function emailVerifier($email, $token)
    {
        try {
            User::where('email', $email)
                    ->where('token', $token)
                    ->update(['email_verified' => 1]);
                
                return 1;
        } catch(Exception $e) { 
            Log::error( 'Error in addCustomer method of User model: ' . $e->getMessage()); 

            return null;
        }

    } 
    
    /**
     * To get user info
     *
     * @param Integer $userId
     * @param String $address
     * @param Integer $phoneNo
     * @param String $userName
     * @param String $firstName
     * @param String $lastName
     * @param String $email
     * 
     * @return 1
     */
    public static function updateInfo($userId, $address, $phoneNo, $firstName, $userName, $lastName, $email)
    {
        try {
            User::where('id', $userId)
                    ->update(['address' => $address, 'phone_no' => $phoneNo, 'first_name' => $firstName, 'user_name' => $userName, 'last_name' => $lastName, 'email' => $email]);
                
                    return 1;
        } catch(Exception $e) { 
            Log::error( 'Error in addCustomer method of User model: ' . $e->getMessage()); 

            return null;
        }
    }

    /**
     * To store socially connected users details in database 
     *
     * @param Object $user
     *
     * @return object
     */
    public static function fetchOrCreateUser($user)
    {
        $name = explode(' ', $user->name);

        return User::firstOrCreate(
            [
                'email' => $user->email
            ], 
            [
                'first_name' => array_first($name),
                'last_name' => end($name),
                'email'    => $user->email,
                'password' => Hash::make('mindfire'),
                'email_verified' => '0',
                'user_name' => $user->name,
            ]
        );
    }

    /**
     * To provide user information through Api
     *
     * @param Object $user
     *
     * @return object
     */
    public static function getUserDetailsFromApi($email)
    {
        try {
            return User::where('email', $email)
                        ->select([
                            'first_name', 
                            'user_name', 
                            'last_name', 
                            'address', 
                            'phone_no', 
                            'gender'
                        ])
                        ->first();
        } catch(Exception $e) {
            Log::error('Error in addCustomer method of User model: ' . $e->getMessage());

            return null;
        }
    }

    /**
     * To provide user information through Api
     *
     * @param Object $user
     *
     * @return integer/null
     */
    public static function findUser($email)
    {
        try {
        return User::where('email', $email)
                    ->select(['id'])
                    ->first();
        } catch(Exception $e) {
            Log::error('Error in addCustomer method of User model: ' . $e->getMessage());

            return null;
        }
    }

    public static function addToken($request, $token)
    {
        User::where('email', $request->email)
              ->update(['forget_password' => $token]) ;
        return 1;
    }

    public static function verifyToken($email, $token)
    {
        return User::where('email', $email)
                    ->where('forget_password', $token)
                    ->select(['id'])
                    ->first();  
    }

    public static function changeCredentials($request)
    {
        return User::where('email', $request->email_id)
                    ->update(['forget_password' => '', 'password' => $request->password]);
    }

    public static function UpdateAddress($request, $id)
    {
        return User::where('id', $id)
                    ->update(['address' => $request->address, 'phone_no' => $request->phoneno]);
    }
}