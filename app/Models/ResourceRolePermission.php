<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceRolePermission extends Model
{
    protected $table ='resource_role_permissions';
}
