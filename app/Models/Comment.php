<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Log;

class Comment extends Model
{
    protected $table = 'comments';

    /**
     * To store comments in database
     *
     * @return object
     */
    public static function storeComment($request, $userId)
    {
        try {
            $comment = new Comment();
            $comment->user_id = $userId;
            $comment->item_id = $request->productId;
            $comment->description = $request->Description;
            $comment->review_point = $request->review_point;
            $comment->save();

            return $comment;
        } catch(Exception $e){
            Log::error('error in addComment in comment model'.$e->getMessage());
            return null;
        }
    }

    /**
     * To Get item details
     *
     * @return object
     */
    public function user()
    {
        try {
            return $this->belongsTo(User::class);
        } catch(Exception $e) { 
            Log::error( 'Error in item method of Shoppingcartitem model: ' . $e->getMessage()); 
            return null;
        }
    }
}
