<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class CheckAuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Route::currentRouteName() == 'register' || Route::currentRouteName() == 'login')
        {
            if(auth::check())
            {
                $returnUrl = redirect()->route('home');
            }
            else
            {
                $returnUrl = $next($request);
            }

            return $returnUrl;
        }
        else
        {
            if($request->ajax())
            {
                if(Auth::check())
                {
                    $returnUrl =  $next($request);
                }

                else
                {
                    $returnUrl = response()->json(array('msg'=> '0'), 200);
                }

                return $returnUrl;
            }
            else
            {
                if(Auth::check())
                {
                    $returnUrl = $next($request);
                }
                else
                {
                    $returnUrl = redirect()->guest('login');
                }

                return $returnUrl;
            }
        }
    }
}
