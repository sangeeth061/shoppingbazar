<?php
namespace App\Http\Middleware;

use Closure;
Use Auth;
Use App\Models\Resource;
use App\Models\Permission;
Use App\Models\ResourceRolePermission;

class AccessVerifier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $resource, $permission)
    {
        $role = Auth::user()->access;
        $resource = Resource::where('name', $resource)->get()->first()->id;
        $permission = Permission::where('type', $permission)->get()->first()->id;

        $result = ResourceRolePermission::where('role_id', $role)
                                        ->where('resource_id', $resource)
                                        ->where('permission_id', $permission)
                                        ->get()->count();

        if($result === 0)
        {
            return redirect('home');
        }
        else
        {
            return $next($request);
        }
    }
}
