<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users;
use Auth;

class CheckAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Auth::user()->address;
        $result2 = Auth::user()->phone_no;

        if(empty($result) || empty($result2))
        {
            return redirect()->route('getAddress');
        }
        else
        {
            return $next($request);
        }
    }
}
