<?php
/**
 * This controller contains methods required to manage orders
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Orderdetails;
use App\Exception;
use App\Exceptions\CustomException;
use App\Models\Shoppingcartitem;
use App\Models\Orderproduct;
use App\Models\Payment;
use App\Models\Shipment;
use App\Models\User;
use \Carbon\Carbon;
use Auth;
use DB; 

/**
 * Class OrderController
 *
 *@package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * To store  order item details in database
     *
     * @access public
     * @param object $request
     * @return view
     */
    public function checkout(Request $request)
    {
        try {
            $userId = Auth::User();
            $items = $userId->cartItems;
            $total = 0;
            $orders = [];
            $increment = 0;
            $time = Carbon::now();
            
            foreach($items as $item)
            {           
                $orders[$increment] = [
                                        'item_name' => $item->item->name, 
                                        'item_id' => $item->item->id, 
                                        'item_cost' => $item->item->cost,
                                        'quantity' => $item->quantity,
                                        'user_id' => $item->user_id,
                                        'created_at' => $time,
                                        'updated_at' => $time,
                                        ] ;   
                $increment++;
            }

            Orderproduct::insert($orders);
            $shipDetails = [];
            $paymentDetails = [];
            $increment = 0;
            
            foreach ($items as $item) 
            {
                $order = Orderproduct::getId($item->item->name, $userId->id);
                $orderId = array_last(array_last($order));
                $shipDetails[$increment] = [
                                                'Orderproduct_id' => $orderId->id,
                                                'user_id' => $userId->id,
                                                'status' => '0',
                                                'created_at' => $time,
                                                'updated_at' => $time,
                                            ];

                $paymentDetails[$increment] = [
                                                    'Orderproduct_id' => $orderId->id,
                                                    'user_id' => $userId->id,
                                                    'status' => '1',
                                                    'created_at' => $time,
                                                    'updated_at' => $time,
                                                    'type' => $request->type,
                                            ];
                $increment++;
            }
            
            Shipment::insert($shipDetails);
            Payment::insert($paymentDetails);
            Shoppingcartitem::destroy($item->id);
            Mail::to($userId->email)->send(new Orderdetails($userId->user_name, $items));
        } catch(Exception $e) {
            Log::error("checkout function in orders controller");
            throw new CustomException($e->getMessage());
        }  

        return redirect('/order')->with('message', 'order placed successfully');
    }

    /**
     * To view orderd items in database
     *
     * @access public
     * @return view
     */
    public function viewOrder()
    {
        try {
            $order = Auth::User();
            $items = $order->orderItems;
            $shipmentStatus = $order->shipItems;
            $paymentStatus = $order->payItems;
        } catch(Exception $e) {
            Log::error("viewOrder function in orders controller");
            throw new CustomException($e->getMessage());
        }  
        
        return view('pages.order',['items'=>$items, 'shipmentStatus'=>$shipmentStatus, 'paymentStatus'=>$paymentStatus]);
    }

    /**
     * To get data for products sale
     *
     * @access public
     * @return object
     */
    public function analysisOrders()
    {
        $item = Orderproduct::productAnalysis();
        return $item;
    }

    /**
     * To view the product salesin website
     *
     * @access public
     * @return view
     */
    public function viewAnalysisOrders()
    {
        return view('pages.productAnalysis');
    }
}