<?php
/**
 * This controller contains methods required to manage payment
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Exceptions\CustomException;
use Auth;
use Log;

/**
 * Class PaymentController
 *
 *@package App\Http\Controllers
 */
class PaymentController extends Controller
{
    /**
     * To show payments page in app
     *
     * @access public
     * @param object $request
     * @return view
     */
    public function show()
    {
        try {
            $cart = Auth::User();
            $items = $cart->cartItems;
            $total = 0;
        } catch(Exception $e) {
            Log::error('in payment controller');
            throw new CustomException($e->getMessage());
        }  

        foreach($items as $item)
        {
            $total += ($item->item->cost * $item->quantity);
        }
    
        return view('pages.payments',['items' => $items, 'total' => $total]);
    }
}