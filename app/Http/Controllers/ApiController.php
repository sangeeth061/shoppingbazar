<?php
/**
 * This controller contains methods required to send the response to the request
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Shoppingcartitem;
use App\Models\Orderproduct;
use Exception;
use Auth;
use Log;

/**
 * Class ApiController
 *
 *@package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * Api to give user Information
     *
     * @param object request
     *
     * @return json
     */
    public function showUserInformation(Request $request)
    {
        try {
            $email = $request->email ?? '';
            $userinfo = User::getUserDetailsFromApi($email);

            return response()->json(['result' => $userinfo, 'message' => 'success',], 200);
        } catch (Exception $e) {
            Log::error('Error in ApiController showUserInformation method : ' . $e->getMessage());
            return response()->json(['result' => [], 'message' => 'fail'], 500);
        }
    }

    /**
     * Api to show user Information
     *
     * @param object request
     *
     * @return json
     */
    public function showOrderInformation(Request $request)
    {
        try {
            $email = $request->email ?? '';
            $userId = User::findUser($email);
            $orderDetails = Orderproduct::getOrderDetailsForApi($userId->id);
            
            return response()->json(['result' => $orderDetails, 'message' => 'success',], 200);
        } catch (Exception $e) {
            Log::error('Error in ApiController showorderInformation method : ' . $e->getMessage());
            
            return response()->json(['result' => [], 'message' => 'fail'], 500);
        }
    }

    /**
     * Api to show shopping cart information
     *
     * @param object request 
     *
     * @return json
     */
    public function showCartInformation(Request $request)
    {
        try {
            $email = $request->email ?? '';
            $userInfo = User::findUser($email);
            $cartDetails = Shoppingcartitem::getCartDetailsForApi($userInfo->id);
            
            return response()->json(['result' => $cartDetails, 'message' => 'success',], 200);
        } catch (Exception $e) {
            Log::error('Error in ApiController showorderInformation method : ' . $e->getMessage());
            
            return response()->json(['result' => [], 'message' => 'fail'], 500);
        }
    }
}