<?php
/**
 * This controller contains methods required to search the item
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Exceptions\CustomException;
use App\Models\Item;
use Log;

/**
 * Class SearchController
 *
 *@package App\Http\Controllers
 */
class SearchController extends Controller
{
    public function show(Request $request)
    {
        $searchItem = $request['search'];

        try {
            $products = Item::search($searchItem);
        } catch(Exception $e) {
            Log::error("In Search controller");
            throw new CustomException($e->getMessage());
        }
        
        return view('pages.search', compact('products'));

    }
}
