<?php
/**
 * This controller contains methods required to logout the user
 *
 */
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

/**
 * Class LogoutController
 *
 *@package App\Http\Controllers
 */
class LogoutController extends Controller
{
    /**
     * To logout user from our website
     *
     * @access public
     * @return view
     */
    public function userLogout()
    {
        Auth::logout();
        
        return redirect('login');
    }
}