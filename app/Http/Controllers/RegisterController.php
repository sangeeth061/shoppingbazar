<?php
/**
 * This controller contains methods required to register the user
 *
 */
namespace App\Http\Controllers;

use App\Http\Requests\ValidatingUser;
use Illuminate\Http\Request;    
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Exception;
use App\Exceptions\CustomException;
use App\Mail\WelcomeMessage;
use Log;

/**
 * Class RegisterController
 *
 *@package App\Http\Controllers
 */
class RegisterController extends Controller
{
    /**
     * Giving access to user for registering
     *
     * @access public
     * @return view
     */
    public function registrationView()
    {
        return view('pages.registration');
    }

    /**
     * To register user in our website
     *
     * @access public
     * @param object $request,$user
     * @return view
     */
    public function addUser(ValidatingUser $request, User $user)
    {
        try {   
            $token = str_random();
            User::addCustomer($request, $user, $token);
        } catch(Exception $e) {
            Log::error("in Register controller");
            throw new CustomException($e->getMessage());
        }  

        Mail::to($request['email'])->send(new WelcomeMessage($request['username'], $request['email'], $token));
        
        return redirect('login')->with('message', 'please check the mail');
    }

    /**
     * Email verification
     *
     * @access public
     * @param object $request,$token
     * @return view
     */
    public function emailVerification($email, $token)
    {
        try {
            User::emailVerifier($email, $token);
        } catch(Exception $e) {
            Log::error("in Register controller");
            throw new CustomException($e->getMessage());
        }  
        
        return redirect('login');
    }
}