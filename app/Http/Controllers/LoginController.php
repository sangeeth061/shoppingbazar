<?php
/**
 * This controller contains methods required to login the user with various sources
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgetPassword;
use App\Models\User;
use GuzzleHttp\Client;
use Hash;
use Auth;

/**
 * Class LoginController
 *
 *@package App\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * To get login view
     *
     * @access public
     * @return view
     */
    public function loginView()
    {
        return view('pages.login');
    }

    /**
     * To login user in our website
     *
     * @access public
     * @param object $request
     * @return view
     */
    public function loginUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'email_verified' => 1,]))
        {
            return redirect()->intended('/home');
        }
        else
        {   
            return redirect('login')->with('message', 'Unauthorized User');
        }
    }

    /**
     * To initate login with google
     *
     * @access public
     * @return object
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * To handle the response send from google
     *
     * @access public
     * @return view
     */
    public function handleProviderCallback()
    {

        $user = Socialite::driver('google')->user();
        $authUser = User::fetchOrCreateUser($user);
        Auth::login($authUser);

        return redirect('/');
    }

    /**
     * To initate login with twitter
     *
     * @access public
     * @return object
     */
    public function redirectToTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * To handle the response send from twitter
     *
     * @access public
     * @return view
     */
    public function handleTwitter()
    {

        $user = Socialite::driver('twitter')->user();
        $authUser = User::fetchOrCreateUser($user);
        Auth::login($authUser);

        return redirect('/');
    }

    /**
     * To handle the response send from shoppingkaroo
     *
     * @access public
     * @param $request object
     *
     * @return view
     */
    public function recieveOauthResponse(Request $request)
    {
        $tempToken = $request->token;
        $client_secret = env('Client_secret');
        $client_id = env('Client_id');
        $client = new Client();

        $res = $client->request('POST', 'http://localhost/shoppingkaroo/public/api/oauth/token/false', [
            'form_params' => [
                'client_id' => $client_id,
                'secret' => $client_secret,
                'token' => $tempToken,
            ]   
        ]);

        $responseResult = json_decode($res->getbody());
        if($responseResult->status == "fail")
        {
            echo "fail";
        }
        else
        {
            $client = new Client();

            $res = $client->request('POST', 'http://localhost/shoppingkaroo/public/api/oauth/token/true', [
                'form_params' => [
                    'token' => $responseResult->status,
                ]   
            ]);
            $response = json_decode($res->getbody());
            $result = $response->status;
            $authUser = User::fetchOrCreateUser($result);
            Auth::login($authUser);
        
            return redirect('/');
        }
    }

    /**
     * To initate shoppingkaroo request for login
     *
     * @access public
     *
     * @return Url
     */
    public function initate()
    {
        return redirect('http://localhost/shoppingkaroo/public/oauth/request/client_id/'.env('Client_id'));
    }

    /**
     * To Handle the forget password request
     *
     * @access public
     * @param $request object
     *
     * @return Url
     */
    public function forget(Request $request)
    {
        $token = str_random();
        User::addToken($request, $token);
        Mail::to($request['email'])->send(new ForgetPassword($request['email'], $token));
        
        return redirect('login')->with('message', "check your mail for futher details");
    }

    /**
     * To verify if its come from authorized user
     *
     * @access public
     * @param $email string
     * @param $token string 
     *
     * @return view
     */
    public function forgetPassword($email, $token)
    {
        $result = User::verifyToken($email, $token);
        if(!empty($result))
        {   
            return view('layouts.forgetPassword', ['email' =>$email]);
        }

    }

    /**
     * To handle change password request
     *
     * @access public
     * @param $request object
     *
     * @return Url
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'email_id' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required_with:password|same:password',
        ]);

        $result = User::changeCredentials($request);

        return redirect('login')->with('message', 'password changed. Login to acess benfits');
    }
}