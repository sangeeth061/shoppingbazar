<?php

/**
 * This controller contains methods required to show and edit the items
 *
 */
namespace App\Http\Controllers;

use App\Http\Requests\ItemVerificationRequest; 
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Exception;
use App\Models\Item;
use App\Models\Comment;
use Log;
use Auth;
use DB;

/**
 * Class ItemsController
 *
 *@package App\Http\Controllers
 */
class ItemsController extends Controller
{
    /**
     * To  Access admin functionalitie
     *
     * @access public
     *
     * @return view
     */
    public function adminView()
    {
        return view('pages.admin');
    }

    /**
     * To add items in our website
     *
     * @access public
     * @param object $request
     *
     * @return view
     */
     public function addItems(ItemVerificationRequest $request)
    {
        ($request->file('image'));
        $formInput = $request->except('image');
        $image = $request->image;

        if($image)
        {
            $imagename = $image->getClientOriginalName();
            $image->storeAs('ItemImages',$imagename,'public');
            $formInput['image'] = $imagename;
        }

        Item::create($formInput);
        
        return redirect()->route('item')->with('message', 'Product Updated');
    }
    
    /**
     * To show view for updating the product
     *
     * @access public
     * @return view
     */
    public function updateView()
    {
        $items = Item::pluck('name', 'id');

        return view('pages.adminUpdate', compact('items'));
    }

    /**
     * To send item detail
     *
     * @access public
     * @param object $request
     * @return json
     */
    public function updateDetails(Request $request )
    {
        try {
            $itemName = $request["item"];
            $items = Item::getItemDetails($itemName);
        } catch(Exception $e) {
            Log::error("in Items controller");
            throw new CustomException($e->getMessage());
        }  

        return response()->json(['items'=> $items], 200);
    }

    /**
     * To store the item details in database
     *
     * @access public
     * @param object $request
     * @return Url;
     */
    public function updateItem(Request $request)
    {
        $id = $request["itemId"];
        $items = Item::getDetails($id);
        $item = $items->image;
        $description = $request["description"];
        $cost = $request["cost"];
        $category_id = $request["category_id"];
        
        if(!empty($request["image"]))
        {
            $image = $request["image"];
            $imagename = $image->getClientOriginalName();
            $image->storeAs('ItemImages',$imagename,'public');
            $image = $imagename;
        }
        else
        {
            $image = $item;
        }

        try {
            Item::itemUpdate($id, $description, $cost, $category_id, $image);
        } catch(Exception $e) {
            Log::error("in Items controller");
            throw new CustomException($e->getMessage());
        }  
        return redirect('/item/create')->with('message', 'Product Updated');
    }

    /**
     * To get category details
     *
     * @access public
     * @param object $request
     * @return json
     */
    public function getCategoryDetails(Request $request)
    {
        try {
            $categoryName = $request["item"];
            $items = Item::viewCategorieProducts($categoryName);
        } catch(Exception $e) {
            Log::error("in Items controller");
            throw new CustomException($e->getMessage());
        }  

        return response()->json(['items'=> $items], 200);
    }

    /**
     * To delete a product/item
     *
     * @access public
     * @param object $request
     * @return json
     */
    public function deleteItem(Request $request)
    {   
        $destroyItem = Item::destroy($request->item);
        return response()->json(['data' => $destroyItem, 200]);
    }

    /**
     * To add feedbacks to item
     *
     * @access public
     * @param object $request
     * @return url
     */
    public function addComment(Request $request)
    {
        $userId = Auth::user()->id;
        $StoreComment = Comment::storeComment($request, $userId);
        $items = Item::viewProduct($request->productId);
        $comments = $items->reviews;
        
        return redirect('item/'.$request->productId);
    }
}