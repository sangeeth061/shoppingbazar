<?php
/**
 * This controller contains methods required to show the cart and add items to cart
 *
 */
namespace App\Http\Controllers;

use App\Http\Requests\ProductUpdateRequest;
use Illuminate\Http\Request;
use App\Models\Shoppingcartitem;
use App\Models\User;
use App\Exceptions\CustomException;
use Exception;
use DB;
use Auth;
use Log;

/**
 * Class CartController
 *
 *@package App\Http\Controllers
 */
class CartController extends Controller
{
    /**
     * To add items in the cart
     *
     * @access public
     * @param productId string
     *
     * @return url
     */
    public function add($productId)
    {
        try {
            $userId = Auth::user()->id;
            Shoppingcartitem::addItemsTocart($productId, $userId);
        } catch(Exception $e) {
            Log::error("add function in Cart controller");
            throw new CustomException($e->getMessage());
        }
        return redirect('/cart');
    }

    /**
     * To display shoppingcart
     *
     * @access public
     *
     * @return view
     */
    public function showCart()
    {
        $userId = Auth::User();
        $items = $userId->cartItems;
        $total = 0;

        foreach($items as $item)
        {
            $total += ($item->item->cost * $item->quantity);
        }

        return view('pages.shoppingCart', ['items' => $items, 'total' => $total]);
    }
    
    /**
     * To remove product
     *
     * @access public
     * @param id string,
     *
     * @return url
     */
    public function remove($id)
    {
        Shoppingcartitem::destroy($id);

        return redirect('/cart');
    }
    
    /**
     * To update quantity of items
     *
     * @access public
     * @param  object request,
     *
     * @return json
     */
    public function updateCart(ProductUpdateRequest $request)
    {
        try {
            $userId = Auth::user()->id;
            $itemQuantity = $request["quantity"];
            $itemId = $request["id"];
            Shoppingcartitem::changeQuantity($userId, $itemQuantity, $itemId);    
        } catch(Exception $e) {
            Log::error("updateCart function in Cart controller");
            throw new CustomException($e->getMessage());
        }            
            return response()->json(array('msg'=> 'success'), 200);
    }

    /**
     * To view all users Shopping cart
     *
     * @access public
     * @return view
     */
    public function viewUserInfo()
    {
        $items = User::all();
        return view('pages.adminUser', compact('items')); 
    }

    /**
     * To view specific user cart
     *
     * @access public
     * @param integer itemId
     *
     * @return view
     */
    public function viewUserCart($itemId)
    {
        try {
            $items = Shoppingcartitem::viewCartDetails($itemId);
        } catch(Exception $e) {
            Log::error("n Cart controller");
            throw new CustomException($e->getMessage());
        }
        
        return view('pages.adminShowCart', compact('items'));
    }
}