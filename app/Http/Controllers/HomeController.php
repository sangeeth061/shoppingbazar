<?php
/**
 * This controller contains methods required to show the home page and other basic pages
 *
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use App\Http\Requests\UpdateVerificationRequest;
use Illuminate\Support\Facades\Mail;
use Exception;
use constants;
use App\Models\Item;
use App\Models\User;
use App\Mail\EmailChange;
use Auth;
use Log;

/**
 * Class HomeController
 *
 *@package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
    * To display homepage
    *
    * @access public
    * @return view
    */
    public function viewHome()
    {
        return view('pages.homePage');
    }

    /**
    * To display pages only with dresses 
    *
    * @access public
    * @return view
    */
    public function viewDress()
    {
        try{
            $products = Item::viewCategory(config('constants.dress'));
        } catch(Exception $e) {  
            Log::error("viewDress method in HomeController");
            throw new CustomException($e->getMessage());
        }  

        return view('pages.dress', compact('products')); 
    }

    /**
    * To display pages with watches
    *
    * @access public
    * @return view
    */
    public function viewWatch()
    {
        try{
            $products = Item::viewCategory(config('constants.watch'));
        } catch(Exception $e) {
            Log::error("viewWatch method in HomeController");
            throw new CustomException($e->getMessage());
        }  


        return view('pages.watches', compact('products')); 
    }

    /**
    * To display pages with electronics
    *
    * @access public
    * @return view
    */
    public function viewElectronic()
    {
        try {
            $products = Item::viewCategory(config('constants.electronics'));
        } catch(Exception $e) 
        {
            Log::error("viewElectronic method in HomeController");
            throw new CustomException($e->getMessage());
        }  
    
      return view('pages.electronics', compact('products'));   
    }

    /**
    * To display specific item
    *
    * @access public
    * @return view
    */
    public function viewItem($productId)
    {
        try {
            $items = Item::viewProduct($productId);
            $comments = $items->reviews;
        } catch(Exception $e) {
            Log::error("viewItem in HomeController");
            throw new CustomException($e->getMessage());
        }  

        return view('pages.item', compact('items', 'comments'));
    }  

    /**
    * To display user profile
    *
    * @access public
    * @return view
    */
    public function viewUser()
    {
        $userDetails = Auth::User();
        
        return view('pages.userInfo', compact('userDetails'));
    }

    /**
    * To edit user profile
    *
    * @access public
    * @return view
    */
    public function editUser()
    {
        $userDetails = Auth::User();
        
        return view('pages.userEdit', compact('userDetails'));
    }

    /**
    * To update user profile in database
    *
    * @access public
    * @return view
    */
    public function editUserInfo(UpdateVerificationRequest $request)
    {
        try{
            $userId = Auth::User()->id;
            
            User::updateInfo($userId, $request['address'], $request['mobileno'], $request['firstname'],  $request['username'], $request['lastname'], $request['email']);
        } catch(Exception $e) {
            Log::error("editUserInfo method in HomeController");
            throw new CustomException($e->getMessage());
        }
        return redirect('/');
    }

    public function orderAddress()
    {
        return view('layouts.shipmentAddress');
    }

    public function updateAddress(Request $request)
    {
        $this->validate($request, [
            'address' => 'required',
            'phoneno' => 'required|min:10|numeric'
        ]);

        $result = User::UpdateAddress($request,Auth::User()->id);
        return redirect();
    }
}