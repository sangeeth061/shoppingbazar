<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Shipment;

class UpdateShipmentStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipments:update-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the status from pending(0) to (1)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Shipment::makeShipment();
    }
}
