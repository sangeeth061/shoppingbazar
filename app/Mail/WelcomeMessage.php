<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $username, $email, $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $email, $token)
    {
        $this->username = $username;
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.registrationmail');
    }
}
