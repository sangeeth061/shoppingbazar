<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Orderdetails extends Mailable
{
    use Queueable, SerializesModels;
    protected $orderItem, $userName;
    public  $items;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName, $items)
    {
        $this->userName = $userName;
        $this->items =$items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.ordersmail')
                    ->with([
                            'username' => $this->userName
                        ]);
    }
}