<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('home');
});

Route::get('register','RegisterController@registrationView')->name('register')->middleware('CheckAuthenticationMiddleware');

Route::post('get/register','RegisterController@addUser')->name('registerd');

Route::get('login','LoginController@loginView')->name('login')->middleware('CheckAuthenticationMiddleware');

Route::post('get/login','LoginController@loginUser')->name('logineed');

Route::get('home','HomeController@viewHome')->name('home');

Route::get('logout','LogoutController@userLogout')->name('logout');

Route::get('admin', 'AdminController@get')->name('admin');

Route::post('item/create', 'ItemsController@addItems')->name('additem')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,create');

Route::get('item/create', 'ItemsController@adminView')->name('item')->middleware('CheckAuthenticationMiddleware','AccessVerifier:item,create');

Route::get('item/update', 'ItemsController@updateView')->name('update')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,update');

Route::post('item/update', 'ItemsController@updateDetails')->name('updateDetails')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,update');

Route::post('item/updateItem', 'ItemsController@updateItem')->name('updateItem')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,update');

Route::post('item/delete', 'ItemsController@deleteItem')->name('delete')->middleware('CheckAuthenticationMiddleware');

Route::get('dress', 'HomeController@viewDress')->name('dress');

Route::get('watches','HomeController@viewWatch')->name('watches');

Route::get('electronics', 'HomeController@viewElectronic')->name('electronics');

Route::get('removeItem/{productId}', 'CartController@remove')->middleware('CheckAuthenticationMiddleware');

Route::get('item/add/{productId}', 'CartController@add')->middleware('CheckAuthenticationMiddleware');

Route::get('cart', 'CartController@showCart')->name('cart')->middleware('CheckAuthenticationMiddleware');

Route::get('item/{itemId}', 'HomeController@viewItem');

Route::post('quantity', 'CartController@updateCart')->middleware('CheckAuthenticationMiddleware');

Route::post('checkout', 'OrderController@checkout')->name('checkout')->middleware('CheckAuthenticationMiddleware', 'CheckAddress');

Route::get('order', 'OrderController@viewOrder')->name('order')->middleware('CheckAuthenticationMiddleware');

Route::get('payments', 'PaymentController@show')->name('payments')->middleware('CheckAuthenticationMiddleware');

Route::post('search', 'SearchController@show')->name('search')->middleware('CheckAuthenticationMiddleware');

Route::get('viewUserCart', 'CartController@viewUserInfo')->name('viewCart')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,create');

Route::get('/cart/{cartId}', 'CartController@viewUserCart')->middleware('CheckAuthenticationMiddleware', 'AccessVerifier:item,create');

Route::get('email/{email}/token/{token}', 'RegisterController@emailVerification');

Route::get('userprofile', 'HomeController@viewUser')->name('user')->middleware('CheckAuthenticationMiddleware');

Route::get('edituserprofile', 'HomeController@editUser')->name('editUser')->middleware('CheckAuthenticationMiddleware');

Route::post('edituserprofile', 'HomeController@editUserInfo')->name('changeUserInfo')->middleware('CheckAuthenticationMiddleware');

Route::get('product/analysis', 'OrderController@analysisOrders');

Route::get('product', 'OrderController@viewAnalysisOrders')->name('analysis');

Route::post('item/updatecategory', 'ItemsController@getCategoryDetails');

Route::get('login/google', 'LoginController@redirectToProvider')->name('socialLogin');

Route::get('login/google/callback', 'LoginController@handleProviderCallback');

//For twitter to give email
Route::get('terms', function(){
	return view('terms');
});


Route::get('login/twitter', 'LoginController@redirectToTwitter');

Route::get('login/twitter/callback', 'LoginController@handleTwitter');

Route::post('addComment', 'ItemsController@addComment')->name('addComment');

//test
Route::get('login/oauth/callback/temptoken', 'LoginController@recieveOauthResponse');

Route::get('login/oauth', 'LoginController@initate');

Route::post('forgetPassword', 'LoginController@forget')->name('forgetPassword');

Route::get('forgetPassword/email/{email}/token/{token}', 'LoginController@forgetPassword');

Route::post('forgetPassword/email/{email}/token/{token}', 'LoginController@changePassword');

Route::get('change/orderAddress', 'HomeController@orderAddress')->name('getAddress');

Route::Post('change/orderAddress', 'HomeController@updateAddress')->name('updateAddress');
