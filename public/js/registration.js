/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/registration.js":
/***/ (function(module, exports) {

var ALPHABETS = new RegExp(/^[a-zA-Z ]+$/);
var NUMBERS = new RegExp(/^[0-9]+$/);
var EMAIL = new RegExp(/^[a-zA-Z][a-z A-Z 0-9]*@[a-zA-z]+\.[a-zA-z]+$/);
var hasError;

$(document).ready(function () {

    $("[name = 'firstname']").blur(function () {
        var firstname = $(this).val();

        if (firstname === "") {
            $("#firstNameError").html("First name is required.");
            hasError = 1;
            return false;
        } else if (!ALPHABETS.test(firstname)) {
            $("#firstNameError").html("Alphabets only.");
            $("[name = 'firstname']").focus();
            hasError = 1;
        } else {
            $("#firstNameError").html("");
        }
    });

    $("[name = 'email']").blur(function () {
        var email1 = $(this).val();

        if (email1 === "") {
            $("#emailError").html("Email is required.");
            $("[name = 'email']").focus();
            hasError = 1;
        } else if (!EMAIL.test(email1)) {
            $("#emailError").html("Invalid email.");
            $("[name = 'email']").focus();
            hasError = 1;
        }
    });

    $("[name = 'password']").keyup(function () {
        var pwd = $(this).val();

        if (pwd === "") {
            $("#passwordError").html("Password is required.");
            $("[name = 'password']").focus();
            hasError = 1;
        } else if (pwd.length < 8) {
            $("#passwordError").html("Min 8 characters.");
            $("[name = 'password']").focus();
            hasError = 1;
        } else {
            $("#passwordError").html("");
        }
    });

    $("[name = 'confirmpass']").keyup(function () {
        var pwd_confirm = $(this).val();

        if (pwd_confirm === "") {
            $("#confirmPasswordError").html("Confirm the password.");
            $("[name = 'confirmpass']").focus();
            hasError = 1;
        } else if (pwd_confirm !== $("[name = 'password']").val()) {
            $("#confirmPasswordError").html("Passwords did NOT match");
            $("[name = 'confirmpass']").focus();
            hasError = 1;
        } else {
            $("#confirmPasswordError").html("");
        }
    });

    $("[name = 'address']").blur(function () {
        var address = $(this).val();

        if (address === "") {
            $("#addressError").html("adress is required");
            $("[name = 'address']").focus();
            hasError = 1;
        } else {
            $("#addressError").html("");
        }
    });

    $("[name = 'phone']").blur(function () {
        var phone = $(this).val();

        if (phone === "") {
            $("#phoneError").html("phone is required.");
            $("[name = 'phone']").focus();
            hasError = 1;
        } else if (phone.length != 10) {
            $("#phoneError").html("invalid no");
            $("[name = 'phone']").focus();
            hasError = 1;
        } else if (!NUMBERS.test(phone)) {
            $("#phoneError").html("invalid no");
            $("[name = 'phone']").focus();
            hasError = 1;
        } else {
            $("#phoneError").html("");
        }
    });
});

function validate() {
    var userName = $('#username').val();
    var firstName = $('#firstname').val();
    var lastName = $('#lastname').val();
    var email = $('#email').val();
    var gender = $('#gender').val();
    var password1 = $('#password').val();
    var password2 = $('#password_confirmation').val();
    var address = $('#address').val();
    var phoneno = $('#phnno').val();

    var hasError = false;

    if (userName === "") {
        $('#ue').html("usernames is required");
        hasError = true;
    } else {
        $('#ue').html("");
    }

    if (firstName === "") {
        $('#firstNameError').html("firstName is required");
        hasError = true;
    } else {
        $('#firstNameError').html("");
    }

    if (userName === "") {
        $('#userNameError').html("username is required");
        hasError = true;
    } else {
        $('#userNameError').html("");
    }

    if (!ALPHABETS.test(firstName)) {
        $('#firstNameError').html("Only Alphabets");
        hasError = true;
    } else {
        $('#firstNameError').html("");
    }

    if (email === "") {
        $('#emailError').html("Email is required");
        hasError = true;
    } else {
        $('#emailError').html("");
    }

    if (!EMAIL.test(email)) {
        hasError = true;
        $('#emailError').html("Invalid email");
    } else {
        $('#emailError').html("");
    }

    if (password1 === "") {
        hasError = true;
        $('#passwordError').html("Required");
    } else {
        $('#passwordError').html("");
    }

    if (password1 != password2) {
        hasError = true;
        $('#confirmPasswordError').html("Password didnt match");
    } else {
        $('#confirmPasswordError').html("");
    }

    if (address === "") {
        hasError = true;
        $('#addressError').html("Address is required");
    } else {
        $('#addressError').html("");
    }

    if (phoneno === "") {
        hasError = true;
        $('#phoneError').html("phone no required");
    } else {
        $('#phoneError').html("");
    }

    if (gender != 'M' && gender != 'F' && gender != 'O') {
        hasError = true;
        $('#genderError').html("Invalid Gender");
    } else {
        $('#genderError').html("");
    }

    return !hasError;
}

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/registration.js");


/***/ })

/******/ });