/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/admin.js":
/***/ (function(module, exports) {

//Get data for the specific item

$(document).ready(function () {
    $('#name').on('input', function () {
        var itemName = $('#name option:selected').text();

        $.ajax({
            type: 'POST',
            url: 'update',
            data: {
                item: itemName
            },

            success: function success(data) {
                var items = data.items;
                var cost = items['cost'];
                var description = items['description'];
                var category = items['category_id'];
                var id = items['id'];
                $('#cost').val(cost);
                $('#description').val(description);
                $('#category_id').val(category);
                $('#category_id option:selected').removeAttr('selected');
                $("#category_id option[value='" + category + "']").attr('selected', 'selected');
                $('#itemId').val(id);
            }
        });
    });
});

//get the inormation about particular category
$(document).ready(function () {
    $('#category_id').on('input', function () {
        var itemName = $("#category_id option:selected").val();

        $.ajax({
            type: 'POST',
            url: 'updatecategory',
            data: {
                item: itemName
            },

            success: function success(data) {
                var items = data.items;
                var name = [];
                var item;
                for (item = 0; item < items.length; ++item) {
                    name[item] = items[item].name;
                }

                var select = document.getElementById('name');
                select.innerHTML = '';
                var fragment = document.createDocumentFragment();
                name.forEach(function (name) {
                    var option = document.createElement('option');
                    option.innerHTML = name;
                    option.value = name;
                    fragment.appendChild(option);
                });
                select.appendChild(fragment);
            }
        });
    });
});

// Validating before sending to server
var NUMBERS = new RegExp(/^[0-9]+$/);
$(document).ready(function () {
    $('#update').click(function () {

        if ($("#name").val() == '1') {
            $('#nameError').html('required');
            return false;
        } else {
            $('#nameError').html('');
        }

        if ($("#description").val() == '') {
            $('#descriptionError').html('required');
            return false;
        } else {
            $('#descriptionError').html('');
        }

        if ($("#cost").val() == '') {
            $('#costError').html('required');
            return false;
        } else if (!NUMBERS.test($('#cost').val())) {
            $('#costError').html('only positive numbers');
            return false;
        } else {
            $('#costError').html('');
        }
    });
});
//To Delete specific item
$(document).ready(function () {
    $('#Delete').click(function () {
        var itemId = $('#itemId').val();
        $.ajax({
            type: 'POST',
            url: 'delete',
            data: {
                item: itemId
            },
            success: function success(data) {
                window.location.replace('update');
            }
        });
    });
});

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/admin.js");


/***/ })

/******/ });