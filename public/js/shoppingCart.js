/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/shoppingCart.js":
/***/ (function(module, exports) {

$(document).ready(function () {

    //to add the quantity
    $('button.add').click(function () {
        var target = $('.quantity', this.parentNode)[0];
        target.value = +target.value + 1;
        quantityUpdate();
        updateTotal();
    });

    //To decrease the quantity
    $('button.sub').click(function () {
        var target = $('.quantity', this.parentNode)[0];
        if (target.value > 1) {
            target.value = +target.value - 1;
            quantityUpdate();
            updateTotal();
        }
    });

    //To update total due to change in quantity
    var updateTotal = function updateTotal() {

        var sum = 0;
        $(".costs").each(function () {
            var price = $(this).data('price');
            var quantity = $(this).parent().parent().find('#quantity').val();

            if (price > 0 && quantity > 0) {
                var subtotal = price * quantity;
                $(this).html(subtotal);
                sum += subtotal;
                $('strong.final').html(sum);
            }
        });
    };

    updateTotal(); //To get intal total

    //To update quantityin database
    function quantityUpdate() {
        $(".quantity").each(function () {
            var quantityNo = $(this).val();
            var itemId = $(this).parent().find('#itemId').val();

            if (quantityNo > 0 && itemId > 0) {
                $.ajax({
                    type: 'POST',
                    url: 'quantity',
                    data: {
                        quantity: quantityNo,
                        id: itemId
                    },

                    success: function success(data) {
                        if (data.msg == "0") {
                            window.location.replace("https://shoppingbazaar.com/public/cart");
                        }
                    }
                });
                $('#error').html("");
            } else {
                $('#error').html("invalid input");
            }
        });
    }
    $('.quantity').on('input', function () {
        quantityUpdate();
        updateTotal();
    });
});

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/shoppingCart.js");


/***/ })

/******/ });